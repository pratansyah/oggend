create table user(
	user_id int(15) primary key auto_increment,
	username varchar(25),
	password varchar(255),
	email varchar(50),
	avatar varchar(255),
	created date,
	modified date,
	token varchar(255),
	token_time datetime,
	fullname varchar(100),
	contact varchar(15),
	location int(10)
	);

create table item(
	item_id int(15) primary key auto_increment,
	user_id int(15),
	category_id int(15),
	title varchar(50),
	price int(15),
	state int(2),
	permalink varchar(100),
	description text,
	type int(2),
	location int(10),
	booked boolean,
	booked_time datetime
	);

create table category(
	category_id int(15) primary key auto_increment,
	name varchar(50),
	parent int(15)
	);

create table image(
	image_id int(15) primary key auto_increment,
	item_id int(15),
	image varchar(100)
	);