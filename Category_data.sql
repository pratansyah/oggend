Mobil
	Mobil Bekas
	Sparepart
	Aksesoris
	Velg dan Ban
	Audio Mobil
Motor
	Motor Bekas
	Sparepart
	Aksesoris
	Helm
Keperluan Pribadi
	Fashion Wanita
		Atasan
		Celana
		Rok
		Busana Muslim
		Dress
		Kebaya
		Baju Tidur
		Baju Hamil
		Pakaian Dalam
		Sepatu & Sandal
		Tas & Dompet
		Aksesoris
	Pakaian Olahraga
	Terapi & Pengobatan
	Fashion Pria
		Baju
		Jas, Jaket, & Sweater
		Celana
		Sandal
		Sepatu
		Aksesoris
	Perhiasan
		Anting
		Gelang
		Cincin
		Batu Permata
		Kalung
		Lain-lain
	Perawatan
		Mandi & Perawatan Tubuh
		Kulit
		Mulut & Gigi
		Mata
		Rambut & Bulu
		Kuku
	Jam Tangan
		Analog
		Digital
	Make Up & Parfum
		Make Up
		Parfum & Minyak Aromatik
	Nutrisi & Suplemen
	Lainnya
Properti
	Rumah
	Bangunan Komersil
	Apartment
	Tanah
	Indekos
	Properti Lainnya
Rumah Tangga
	Makanan & Minuman
	Furniture
	Dekorasi Rumah
	Konstruksi & Taman
	Jam
	Lampu
	Perlengkapan Rumah
		Perlengkapan & Tempat Tidur
		Kamar Mandi
		Dapur
		Perlengkapan Makan
	Lain-lain
Elektronik
	Handphone
		Apple
		Blackberry
		HTC
		Huawei
		Lenovo
		LG
		Motorola
		Nexian
		Nokia
		Samsung
		Sony
		Lain-lain
	Tablet
		Acer
		Advan
		Apple
		Asus
		Hueawei
		Samsung
		Smartfren
		Lain-lain
	Aksesoris HP & Tablet
		Casing
		Baterai
		Charger
		Headset
		Karut Memory
		Kartu Sim
		Aksesoris Lainnya
	Fotografi
		Kamera Digital
		Kamera DSLR
		Lensa
		Perlengkapan Kmaera Pro
		Aksesoris
		Teleskop
		Kamera Lainnya
	Elektronik Rumah Tangga
		Peralatan Elektronik
		Mesin Cuci
		Alat Dapur
		Kulkas & Frezer
		Kipas Angin, AC & Exhaust
		Alat Listrik
		Telekom
		Lain-lain
	Games & Console
		Console
		Portable
		Games
		Aksesoris
		Reparasi & Perbaikan
	Komputer
		Laptop
		Komputer Desktop
		Printer, Tinta & Scanner
		Monitor
		Keyborad & Mouse
		Komponen
		Memory
		Modem & Router
		Flashdisk
		Hard Disk
		Proyektor
		Kamera
		Software
		Aksesoris
		Server
	Lampu
	TV & Audio, Video
		Televisi
		Home Theater & Speaker
		Video Player
		Pemutar Musik Portable
		Audio Player & Rec
		Headphone
		Kamera Video
		Antena
		Aksesoris & Kabel
Hobi
	Alat-alat Musik
		Gitar & Bass
		Efek Gitar & Bass
		Keyboard & Piano
		Drum & Perkusi
		Alat Musik Tiup
		Alat mUsik Tradisional
		Mixer & Microphones
		Studio & Panggung
		Aksesoris Musik
		Lainnya
	Olahraga
		Sepakbola & Futsal
		Basket
		Olahraga Air
		Bulutangkis & Tennis
		Peralatan Fitness
		Skateboard & Sepatu Roda
		Hiking & Panjat Tebing
		Beladiri
		Memancing
		Olahraga Lainnya
	Sepeda & Aksesoris
		Sepeda Gunung
		Sepeda Anak
		Sepeda BMX
		Sepeda Fixie
		Sepeda Lipat
		Aksesoris
		Suku Cadang
		Sepeda Lainnya
	Handicrafts
		Bambu, Kayu & Rotan
		Barang Daur Ulang
		Barang Pecah Belah
		Batu-batuan
		Logan & Perhiasan
		Ragam
	Barang Antik
	Buku & Majalah
	Koleksi
	Mainan Hobi
	Musik & Film
	Hewan Peliharaan
Bayi & Anak

INSERT INTO category(name, parent) VALUES('Mobil', 0);
insert into category(name, parent) values('Motor', 0);
insert into category(name, parenT) values('Keperluan Pribadi', 0);
insert into category(name, parent) values('Properti', 0);
insert into category(name, parent) values('Rumah Tangga', 0);
insert into category(name, parent) values('Elektronik', 0);
insert into category(name, parent) values('Hobi', 0);
insert into category(name, parent) values('Bayi & Anak', 0);

INSERT INTO category(name, parent) values('Mobil Bekas', 1);
INSERT INTO category(name, parent) values('Sparepart', 1);
INSERT INTO category(name, parent) values('Aksesoris', 1);
INSERT INTO category(name, parent) values('Velg & Ban', 1);
INSERT INTO category(name, parent) values('Audio Mobil', 1);

INSERT INTO category(name, parent) values('Motor Bekas', 2);
INSERT INTO category(name, parent) values('Sparepart', 2);
INSERT INTO category(name, parent) values('Aksesoris', 2);
INSERT INTO category(name, parent) values('Helm', 2);

INSERT INTO category(name, parent) values('Fashion Wanita', 3);
INSERT INTO category(name, parent) values('Pakaian Olahraga', 3);
INSERT INTO category(name, parent) values('Terapi & Pengobatan', 3);
INSERT INTO category(name, parent) values('Fashion Pria', 3);
INSERT INTO category(name, parent) values('Perhiasan', 3);
INSERT INTO category(name, parent) values('Perawatan', 3);
INSERT INTO category(name, parent) values('Jam Tangan', 3);
INSERT INTO category(name, parent) values('Make Up & Parfum', 3);
INSERT INTO category(name, parent) values('Nutrisi & Suplemen', 3);
INSERT INTO category(name, parent) values('Lainnya', 3);

INSERT INTO category(name, parent) values('Atasan', 18);
INSERT INTO category(name, parent) values('Celana', 18);
INSERT INTO category(name, parent) values('Rok', 18);
INSERT INTO category(name, parent) values('Dress', 18);
INSERT INTO category(name, parent) values('Kebaya', 18);
INSERT INTO category(name, parent) values('Baju Tidur', 18);
INSERT INTO category(name, parent) values('Baju Hamil', 18);
INSERT INTO category(name, parent) values('Pakaian Dalam', 18);
INSERT INTO category(name, parent) values('Sepatu & Sandal', 18);
INSERT INTO category(name, parent) values('Tas & Dompet', 18);
INSERT INTO category(name, parent) values('Aksesoris', 18);

INSERT INTO category(name, parent) values('Baju', 21);
INSERT INTO category(name, parent) values('Jas, Jaket, & Sweater', 21);
INSERT INTO category(name, parent) values('Celana', 21);
INSERT INTO category(name, parent) values('Sandal', 21);
INSERT INTO category(name, parent) values('Sepatu', 21);
INSERT INTO category(name, parent) values('Aksesoris', 21);

INSERT INTO category(name, parent) values('Analog', 24);
INSERT INTO category(name, parent) values('Digital', 24);

INSERT INTO category(name, parent) values('Anting', 22);
INSERT INTO category(name, parent) values('Gelang', 22);
INSERT INTO category(name, parent) values('Cincin', 22);
INSERT INTO category(name, parent) values('Batu Permata', 22);
INSERT INTO category(name, parent) values('Kalung', 22);
INSERT INTO category(name, parent) values('Lain-lain', 22);

INSERT INTO category(name, parent) values('Mandi & Perawatan Tubuh', 23);
INSERT INTO category(name, parent) values('Kulit', 23);
INSERT INTO category(name, parent) values('Mulut & Gigi', 23);
INSERT INTO category(name, parent) values('Mata', 23);
INSERT INTO category(name, parent) values('Rambut & Bulu', 23);
INSERT INTO category(name, parent) values('Kuku', 23);


INSERT INTO category(name, parent) values('Make Up', 25);
INSERT INTO category(name, parent) values('Parfum & Minyak Aromatik', 25);

INSERT INTO category(name, parent) values('Rumah', 4);
INSERT INTO category(name, parent) values('Bangunan Komersil', 4);
INSERT INTO category(name, parent) values('Apartment', 4);
INSERT INTO category(name, parent) values('Tanah', 4);
INSERT INTO category(name, parent) values('Indekos', 4);
INSERT INTO category(name, parent) values('Properti Lainnya', 4);

INSERT INTO category(name, parent) values('Makanan & Minuman', 5);
INSERT INTO category(name, parent) values('Furniture', 5);
INSERT INTO category(name, parent) values('Dekorasi Rumah', 5);
INSERT INTO category(name, parent) values('Konstruksi & Taman', 5);
INSERT INTO category(name, parent) values('Jam', 5);
INSERT INTO category(name, parent) values('Lampu', 5);
INSERT INTO category(name, parent) values('Perlengkapan Rumah', 5);
INSERT INTO category(name, parent) values('Lain-lain', 5);

INSERT INTO category(name, parent) values('Perlengkapan & Tempat Tidur', 73);
INSERT INTO category(name, parent) values('Kamar Mandi', 73);
INSERT INTO category(name, parent) values('Dapur', 73);
INSERT INTO category(name, parent) values('Perlengkapan Makan', 73);

INSERT INTO category(name, parent) values('Handphone', 6);
INSERT INTO category(name, parent) values('Tablet', 6);
INSERT INTO category(name, parent) values('Aksesoris HP & Tablet', 6);
INSERT INTO category(name, parent) values('Fotografi', 6);
INSERT INTO category(name, parent) values('Elektronik Rumah Tangga', 6);
INSERT INTO category(name, parent) values('Games & Console', 6);
INSERT INTO category(name, parent) values('Komputer', 6);
INSERT INTO category(name, parent) values('Lampu', 6);
INSERT INTO category(name, parent) values('TV & Audio, Video', 6);

INSERT INTO category(name, parent) values('Apple', 79);
INSERT INTO category(name, parent) values('Blackberry', 79);
INSERT INTO category(name, parent) values('HTC', 79);
INSERT INTO category(name, parent) values('Huawei', 79);
INSERT INTO category(name, parent) values('Lenovo', 79);
INSERT INTO category(name, parent) values('LG', 79);
INSERT INTO category(name, parent) values('Motorola', 79);
INSERT INTO category(name, parent) values('Nexian', 79);
INSERT INTO category(name, parent) values('Nokia', 79);
INSERT INTO category(name, parent) values('Samsung', 79);
INSERT INTO category(name, parent) values('Sony', 79);
INSERT INTO category(name, parent) values('Lain-lain', 79);

INSERT INTO category(name, parent) values('Acer', 80);
INSERT INTO category(name, parent) values('Advan', 80);
INSERT INTO category(name, parent) values('Apple', 80);
INSERT INTO category(name, parent) values('Asus', 80);
INSERT INTO category(name, parent) values('Huawei', 80);
INSERT INTO category(name, parent) values('Samsung', 80);
INSERT INTO category(name, parent) values('Smartfren', 80);
INSERT INTO category(name, parent) values('Lain-lain', 80);

INSERT INTO category(name, parent) values('Casing', 81);
INSERT INTO category(name, parent) values('Baterai', 81);
INSERT INTO category(name, parent) values('Charger', 81);
INSERT INTO category(name, parent) values('Headset', 81);
INSERT INTO category(name, parent) values('Kartu Memory', 81);
INSERT INTO category(name, parent) values('Kartu Sim', 81);
INSERT INTO category(name, parent) values('Aksesoris Lainnya', 81);

INSERT INTO category(name, parent) values('Kamera Digital', 82);
INSERT INTO category(name, parent) values('Kamera DSLR', 82);
INSERT INTO category(name, parent) values('Lensa', 82);
INSERT INTO category(name, parent) values('Perlengkapan Kamera Pro', 82);
INSERT INTO category(name, parent) values('Aksesoris', 82);
INSERT INTO category(name, parent) values('Teleskop', 82);
INSERT INTO category(name, parent) values('Kamera Lainnya', 82);

INSERT INTO category(name, parent) values('Peralatan Elektronik', 83);
INSERT INTO category(name, parent) values('Mesin Cuci', 83);
INSERT INTO category(name, parent) values('Alat Dapur', 83);
INSERT INTO category(name, parent) values('Kulkas & Freezer', 83);
INSERT INTO category(name, parent) values('Kipas Angin, AC & Exhaust', 83);
INSERT INTO category(name, parent) values('Alat Listrik', 83);
INSERT INTO category(name, parent) values('Telekom', 83);
INSERT INTO category(name, parent) values('Lain-lain', 83);

INSERT INTO category(name, parent) values('Console', 84);
INSERT INTO category(name, parent) values('Portable', 84);
INSERT INTO category(name, parent) values('Games', 84);
INSERT INTO category(name, parent) values('Aksesoris', 84);
INSERT INTO category(name, parent) values('Reparasi & Perbaikan', 84);

INSERT INTO category(name, parent) values('Laptop', 85);
INSERT INTO category(name, parent) values('Komputer Desktop', 85);
INSERT INTO category(name, parent) values('Printer, Tinta & Scanner', 85);
INSERT INTO category(name, parent) values('Monitor', 85);
INSERT INTO category(name, parent) values('Keyborad & Mouse', 85);
INSERT INTO category(name, parent) values('Komponen', 85);
INSERT INTO category(name, parent) values('Memory', 85);
INSERT INTO category(name, parent) values('Modem & Router', 85);
INSERT INTO category(name, parent) values('Flashdisk', 85);
INSERT INTO category(name, parent) values('Hard Disk', 85);
INSERT INTO category(name, parent) values('Proyektor', 85);
INSERT INTO category(name, parent) values('Kamera', 85);
INSERT INTO category(name, parent) values('Software', 85);
INSERT INTO category(name, parent) values('Aksesoris', 85);
INSERT INTO category(name, parent) values('Server', 85);

INSERT INTO category(name, parent) values('Televisi', 87);
INSERT INTO category(name, parent) values('Home Theater & Speaker', 87);
INSERT INTO category(name, parent) values('Video Player', 87);
INSERT INTO category(name, parent) values('Pemutar Musik Portable', 87);
INSERT INTO category(name, parent) values('Audio Player & Rec', 87);
INSERT INTO category(name, parent) values('Headphone', 87);
INSERT INTO category(name, parent) values('Kamera Video', 87);
INSERT INTO category(name, parent) values('Antena', 87);
INSERT INTO category(name, parent) values('Aksesoris & Kabel', 87);

INSERT INTO category(name, parent) values('Alat-alat Musik', 7);
INSERT INTO category(name, parent) values('Olahraga', 7);
INSERT INTO category(name, parent) values('Sepeda & Aksesoris', 7);
INSERT INTO category(name, parent) values('Handicrafts', 7);
INSERT INTO category(name, parent) values('Barang Antik', 7);
INSERT INTO category(name, parent) values('Buku & Majalah', 7);
INSERT INTO category(name, parent) values('Koleksi', 7);
INSERT INTO category(name, parent) values('Mainan Hobi', 7);
INSERT INTO category(name, parent) values('Musik & Film', 7);
INSERT INTO category(name, parent) values('Hewan Peliharaan', 7);

INSERT INTO category(name, parent) values('Gitar & Bass', 159);
INSERT INTO category(name, parent) values('Efek Gitar & Bass', 159);
INSERT INTO category(name, parent) values('Keyboard & Piano', 159);
INSERT INTO category(name, parent) values('Drum & Perkusi', 159);
INSERT INTO category(name, parent) values('Alat Musik Tiup', 159);
INSERT INTO category(name, parent) values('Alat Musik Tradisional', 159);
INSERT INTO category(name, parent) values('Mixer & Microphones', 159);
INSERT INTO category(name, parent) values('Studio & Panggung', 159);
INSERT INTO category(name, parent) values('Aksesoris Musik', 159);
INSERT INTO category(name, parent) values('Lainnya', 159);

INSERT INTO category(name, parent) values('Sepakbola & Futsal', 160);
INSERT INTO category(name, parent) values('Basket', 160);
INSERT INTO category(name, parent) values('Olahraga Air', 160);
INSERT INTO category(name, parent) values('Bulutangkis & Tennis', 160);
INSERT INTO category(name, parent) values('Peralatan Fitness', 160);
INSERT INTO category(name, parent) values('Skateboard & Sepatu Roda', 160);
INSERT INTO category(name, parent) values('Hiking & Panjat Tebing', 160);
INSERT INTO category(name, parent) values('Beladiri', 160);
INSERT INTO category(name, parent) values('Memancing', 160);
INSERT INTO category(name, parent) values('Olahraga Lainnya', 160);

INSERT INTO category(name, parent) values('Sepeda Gunung', 161);
INSERT INTO category(name, parent) values('Sepeda Anak', 161);
INSERT INTO category(name, parent) values('Sepeda BMX', 161);
INSERT INTO category(name, parent) values('Sepeda Fixie', 161);
INSERT INTO category(name, parent) values('Sepeda Lipat', 161);
INSERT INTO category(name, parent) values('Aksesoris', 161);
INSERT INTO category(name, parent) values('Suku Cadang', 161);
INSERT INTO category(name, parent) values('Sepeda Lainnya', 161);

INSERT INTO category(name, parent) values('Bambu, Kayu & Rotan', 162);
INSERT INTO category(name, parent) values('Barang Daur Ulang', 162);
INSERT INTO category(name, parent) values('Barang Pecah Belah', 162);
INSERT INTO category(name, parent) values('Batu-batuan', 162);
INSERT INTO category(name, parent) values('Logam & Perhiasan', 162);
INSERT INTO category(name, parent) values('Ragam', 162);

INSERT INTO category(name, parent) values('Agama & Kerohanian', 164);
INSERT INTO category(name, parent) values('Biografi & Kenangan', 164);
INSERT INTO category(name, parent) values('Buku Anak-anak', 164);
INSERT INTO category(name, parent) values('Buku Import', 164);
INSERT INTO category(name, parent) values('Buku Sekolah', 164);
INSERT INTO category(name, parent) values('Desain/Arsitektur', 164);
INSERT INTO category(name, parent) values('Ekonomi Bisnis', 164);
INSERT INTO category(name, parent) values('Hukum & Psikologi', 164);
INSERT INTO category(name, parent) values('Ilmu Pengetahuan', 164);
INSERT INTO category(name, parent) values('Kamus & Ensiklopedia', 164);
INSERT INTO category(name, parent) values('Kesehatan, Jiwa & Raga', 164);
INSERT INTO category(name, parent) values('Komputer & Internet', 164);
INSERT INTO category(name, parent) values('Lain-lain', 164);
INSERT INTO category(name, parent) values('Lifestyle', 164);
INSERT INTO category(name, parent) values('Majalah', 164);
INSERT INTO category(name, parent) values('Novel & Komik', 164);
INSERT INTO category(name, parent) values('Orang Tua & Keluarga', 164);
INSERT INTO category(name, parent) values('Sastra & Fiksi', 164);
INSERT INTO category(name, parent) values('Sejarah', 164);
INSERT INTO category(name, parent) values('Sosial & Politik', 164);
INSERT INTO category(name, parent) values('Teknik & Keahlian', 164);

INSERT INTO category(name, parent) values('Aneka Barang Kesenangan', 165);
INSERT INTO category(name, parent) values('Anime', 165);
INSERT INTO category(name, parent) values('Batuan Fosil', 165);
INSERT INTO category(name, parent) values('Benda Pos', 165);
INSERT INTO category(name, parent) values('Jam', 165);
INSERT INTO category(name, parent) values('Kaset & Piringan Hitam', 165);
INSERT INTO category(name, parent) values('Koleksi Orang Terkenal', 165);
INSERT INTO category(name, parent) values('Militeristik', 165);
INSERT INTO category(name, parent) values('Sticker & Gantungan Kunci', 165);
INSERT INTO category(name, parent) values('Uang & Koin', 165);
INSERT INTO category(name, parent) values('Lain-lain', 165);

INSERT INTO category(name, parent) values('Action Figure', 166);
INSERT INTO category(name, parent) values('Miniatur/Die Cast', 166);
INSERT INTO category(name, parent) values('Radio Control', 166);

INSERT INTO category(name, parent) values('Anjing', 168);
INSERT INTO category(name, parent) values('Kucing', 168);
INSERT INTO category(name, parent) values('Burung', 168);
INSERT INTO category(name, parent) values('Hewan Pengerat', 168);
INSERT INTO category(name, parent) values('Ikan', 168);
INSERT INTO category(name, parent) values('Reptil', 168);
INSERT INTO category(name, parent) values('Hewan Lainnya', 168);
INSERT INTO category(name, parent) values('Dokter & Perawatan', 168);
INSERT INTO category(name, parent) values('Makanan & Aksesoris', 168);

INSERT INTO category(name, parent) values('Pakaian', 8);
INSERT INTO category(name, parent) values('Perlengkapan Bayi', 8);
INSERT INTO category(name, parent) values('Perlengkapan Ibu Bayi', 8);
INSERT INTO category(name, parent) values('Boneka & Mainan Anak', 8);
INSERT INTO category(name, parent) values('Buku Anak-anak', 8);
INSERT INTO category(name, parent) values('Stroller', 8);
INSERT INTO category(name, parent) values('Lain-lain', 8);

INSERT INTO category(name, parent) values('Sehari-hari', 248);
INSERT INTO category(name, parent) values('Lainnya', 248);