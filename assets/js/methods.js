var $ = jQuery.noConflict();
$(document).ready(function(){

	$(".video").fitVids();


	// tinymce text editor -->
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media contextmenu paste"
	    ],
	    setup: function(editor) {
	    	editor.on('change', function(e){
	    		$('#hidden_deskripsi').val(tinyMCE.activeEditor.getContent({format : 'raw'}));
	    	});
	    },
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});

	// slider thumbnail -->
	$(".slider-product").owlCarousel({
		items : 6,
		navigation : true
	});



	Dropzone.options.avatarUpload = {
	  paramName: "file", // The name that will be used to transfer the file
	  maxFilesize: 2, // MB
	  accept: function(file, done) {
	    if (file.name == "justinbieber.jpg") {
	      done("Naha, you don't.");
	    }
	    else { done(); }
	  }
	};

	// CUSTOM SELECT STYLE
	function custom_select(select_id) {
		var $this = $(select_id);
		var numberOfOptions = $this.children('option').length;
	  	var data = $this.data();
	  	var function_name = $this.data('function');
	    $this.addClass('select-hidden'); 
	    $this.wrap('<div class="select"></div>');
	    $this.after('<div class="select-styled"></div>');

	    var $styledSelect = $this.next('div.select-styled');
	    $styledSelect.text($this.children('option').eq(0).text());
	  
	    var $list = $('<ul />', {
	        'class': 'select-options'
	    }).insertAfter($styledSelect);
	  
	    for (var i = 0; i < numberOfOptions; i++) {
	        $('<li />', {
	            text: $this.children('option').eq(i).text(),
	            rel: $this.children('option').eq(i).val()
	        }).appendTo($list);
	    }
	  
	    var $listItems = $list.children('li');
	  
	    $styledSelect.click(function(e) {
	        e.stopPropagation();
	        $('div.select-styled.active').each(function(){
	            $(this).removeClass('active').next('ul.select-options').hide();
	        });
	        $(this).toggleClass('active').next('ul.select-options').toggle();
	        
	    });
	  
	    $listItems.click(function(e) {
	        e.stopPropagation();
	        $styledSelect.text($(this).text()).removeClass('active');
	        $this.val($(this).attr('rel'));
	        $list.hide();
	        if(function_name){
	       		select_function[function_name](data, $this.val(), $this);
	        }
	    });
	  
	    $(document).click(function() {
	        $styledSelect.removeClass('active');
	        $list.hide();

	    });
		$(".select-options").mCustomScrollbar();
	}
	$('select').each(function(){

	    var $this = $(this), numberOfOptions = $(this).children('option').length;
	  	var data = $this.data();
	  	var function_name = $this.data('function');
	    $this.addClass('select-hidden'); 
	    $this.wrap('<div class="select"></div>');
	    $this.after('<div class="select-styled"></div>');

	    var $styledSelect = $this.next('div.select-styled');
	    $styledSelect.text($this.children('option').eq(0).text());
	  
	    var $list = $('<ul />', {
	        'class': 'select-options'
	    }).insertAfter($styledSelect);
	  
	    for (var i = 0; i < numberOfOptions; i++) {
	        $('<li />', {
	            text: $this.children('option').eq(i).text(),
	            rel: $this.children('option').eq(i).val()
	        }).appendTo($list);
	    }
	  
	    var $listItems = $list.children('li');
	  
	    $styledSelect.click(function(e) {
	        e.stopPropagation();
	        $('div.select-styled.active').each(function(){
	            $(this).removeClass('active').next('ul.select-options').hide();
	        });
	        $(this).toggleClass('active').next('ul.select-options').toggle();
	        
	    });
	  
	    $listItems.click(function(e) {
	        e.stopPropagation();
	        $styledSelect.text($(this).text()).removeClass('active');
	        $this.val($(this).attr('rel'));
	        $list.hide();
	        if(function_name){
	       		select_function[function_name](data, $this.val(), $this);
	        }
	    });
	  
	    $(document).click(function() {
	        $styledSelect.removeClass('active');
	        $list.hide();

	    });

	});
	$(".select-options").mCustomScrollbar();

	// PILIH KATEGORI
	$(".close").click(function() {
    	$(".kategori-iklan").removeClass("opened")
        $(".kategori-iklan").addClass("closed");
    	// $(".kategori-iklan").css("display", "none");
    });
    $(".pilih-kategori").click(function() {
    	$(".kategori-iklan").css("display", "block");
    	$(".kategori-iklan").removeClass("closed")
    	$(".kategori-iklan").addClass("opened");
    });


	// SET BUTTON "CARI" HEIGHT EQUAL TO 
	$(".detail-sorter .button").css({'height':($(".detail-sorter").height()+'px')});

	// PINTEREST GRID DI HOME
	$('.pin-grid').pinterest_grid({
        no_columns: 4,
        padding_x: 20,
        padding_y: 20,
        margin_bottom: 50,
        single_column_breakpoint: 680
    });

	// ZOOM EFFECT GALLERY
	$("#zoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'}); 

	// TOGGLEING VIEW BUTTON
	$('.view-option.thumbnail-view').addClass('view-active');
	$('.view-option').click(function(event) {
		$('.view-option').removeClass('view-active');
		$(this).addClass('view-active');
	});



	$('.catalog-products ul').addClass('view-thumbnail');

	$('.view-option.list-view').click(function(event) {
		$('.catalog-products ul').removeClass('view-thumbnail');
		$('.catalog-products ul').addClass('view-list');
	});
	$('.view-option.thumbnail-view').click(function(event) {
		$('.catalog-products ul').removeClass('view-list');
		$('.catalog-products ul').addClass('view-thumbnail');
	});

	$('.register-form #full-name, .register-form #username, .register-form #no_hp, .register-form #email, .register-form #password, .register-form #password_repeat, .register-form #c1').change(function(){
		check_completeness();
	});

	$('.register-form #full-name, .register-form #username, .register-form #no_hp, .register-form #email, .register-form #password, .register-form #password_repeat, .register-form #c1').click(function(){
		check_completeness();
	});

	$('.register-form #full-name, .register-form #username, .register-form #no_hp, .register-form #email, .register-form #password, .register-form #password_repeat, .register-form #c1').keyup(function(){
		check_completeness();
	});



	function check_completeness() {
		elements = ["#full-name", "#username", "#no_hp", "#email", "#password", "#password_repeat"];
		color = "yellow";
		disabled = false;
		elements.forEach(function(entry){
			if(!$(entry).val()){
				color = '';
				$('#submit_signup').removeClass('yellow');
				disabled = true;
			}

			if(!$('#c1').is(':checked')){
				color = '';
				$('#submit_signup').removeClass('yellow');
				disabled = true;
			}
		});
		$('#submit_signup').attr('disabled', disabled);
		$('#submit_signup').addClass(color);
	}
	var h = '';
	for (i = 1; i < 200; i++) h += '<option value='+i+'>'+i;
	$('#lokasi').html(h).val(40).scrollTop(160);



	var select_function={
		sort: function(param, value){
			// console.log(param.url+'?sort='+value);
			window.location=param.url+'?sort='+value;
		},
		load_kabupaten: function(param, value, elem){
			$('#hidden_lokasi').val(value);
			// console.log(elem.children('option[value="'+value+'"]').data('province_code'));
			$.ajax({
				url: $('body').data('base_url')+'ajax/load_city/'+elem.children('option[value="'+value+'"]').data('province_code'),
				dataType: 'json'
			})
			.success(function(data){
				generate_city_select(data);
			});
		},
		assign_value: function(param, value){
			$('#hidden_'+param.id).val(value);
		}

	}

	function generate_city_select(data) {
		if(data.status){
			initial_select = '<select id="kabupaten" name="kabupaten" class="kabupaten_anchor" data-function="assign_value" data-id="lokasi"><option value="hide">pilih kabupaten</option></div>'
			$('.kabupaten_row .controls').html(initial_select);
			$.each(data.cities, function(){
				$('.kabupaten_anchor').append('<option value="'+this.location_id+'">'+this.location_name+'</option>');
			});
			custom_select('.kabupaten_anchor');
			$('.kabupaten_row').css('display', 'block');
		}
	}

	$(".iklan-action .submit").click(function(event){
		    $(this).addClass("succeed");
	});

	// alert
	$("#success").click(function () {
	  $(".notify").toggleClass("active");
	  $("#notifyType").toggleClass("success");
	  
	  setTimeout(function(){
	    $(".notify").removeClass("active");
	    $("#notifyType").removeClass("success");
	  },2000);
	});

	$("#failure").click(function () {
	  $(".notify").addClass("active");
	  $("#notifyType").addClass("failure");
	  
	  setTimeout(function(){
	    $(".notify").removeClass("active");
	    $("#notifyType").removeClass("failure");
	  },2000);
	});
	// $('.login-wrap').click(function() {
	// 	$(this).fadeOut();
	// 	});
	// 	$('.login-form').click(function(event){
	// 	    event.stopPropagation();
	// });

	$('.iklan-form :input').change(function(){
		$('#hidden_'+$(this).attr('name')).val($(this).val());
	});

	function custom_alert(status, message) {
		if(status == true) {
			var alert_type='success';
		}
		else if(status == false) {
			var alert_type = 'failure';
		}
		$(".notify #notifyType").html(message);
		$(".notify").toggleClass("active");
		$("#notifyType").toggleClass(alert_type);

		setTimeout(function(){
			$(".notify").removeClass("active");
			$("#notifyType").removeClass(alert_type);
		},2000);
	}

	$('.register-form #username').change(function(){
		$.ajax({
			url: $('body').data('base_url')+'ajax/check_username/'+$(this).val(),
			dataType: 'json'
		})
		.success(function(data){
			custom_alert(data.status, data.string);
		});
	});

	$('.pasang-iklan .iklan-images .uploader input[type=file]').change(function(){
		$(this).css('background-color', 'black');
	});

	$('#book').click(function(){
		$.ajax({
			url: $('body').data('base_url')+'ajax/book/',
			dataType: 'json',
			data: {token: $(this).data('token'), item_id: $(this).data('item_id')},
			type: 'post'
		})
		.success(function(data){
			if(data.status == true){
				$('#book').attr('style', 'background: grey; color: white;');
				$('#book').html('Telah dipesan');
				$('#book').unbind('click');
				custom_alert(true, 'Barang berhasil dipesan');
			}
			else if(data.status == 'not_login') {
				custom_alert(false, 'Silahkan login');
			}
			else if(data.status == 'same_user') {
				custom_alert(false, 'Tidak bisa membeli barang sendiri');
			}
		});
	});

	$('.category-level1 li a').click(function(){
		$('.category-level1 li a').each(function(){
			$(this).css('color', 'white');
		});
		$(this).css('color', '#ded021');
		$('#hidden_kategori').val($(this).data('key'));
			$('.pilih-kategori').html($(this).html());
		$.ajax({
			url: $('body').data('base_url')+'ajax/load_child_categories/'+$(this).data('key'),
			dataType: 'json'
		})
		.success(function(data){
			$('.category-level2').html('');
			$('.category-level3').html('');
			var content = '';
			$.each(data, function(){
				content += '<li><a href="#" data-key="'+this.category_id+'"><font>'+this.name+'</font></a></li>';
			});
			$('.category-level2').html(content);
			bind_level2();

		});
	});	

	function bind_level2() {
		$('.category-level2 li a').click(function(){
			$('.category-level2 li a').each(function(){
				$(this).css('color', 'white');
			});
			$(this).css('color', '#ded021');
			$('#hidden_kategori').val($(this).data('key'));
			$('.pilih-kategori').html($(this).html());
			$.ajax({
				url: $('body').data('base_url')+'ajax/load_child_categories/'+$(this).data('key'),
				dataType: 'json'
			})
			.success(function(data){
				$('.category-level3').html('');
				var content = '';
				$.each(data, function(){
					content += '<li><a href="#" data-key="'+this.category_id+'"><font>'+this.name+'</font></a></li>';
				});
				$('.category-level3').html(content);
				bind_level3()

			});
		});
	}

	function bind_level3() {
		$('.category-level3 li a').click(function(){
			$('.category-level3 li a').each(function(){
				$(this).css('color', 'white');
			});
			$(this).css('color', '#ded021');
			$('#hidden_kategori').val($(this).data('key'));
			$('.pilih-kategori').html($(this).html());
		});
	}

	$('.edit-iklan .edit').click(function(){
		window.location=$('body').data('base_url')+'dashboard/edit_ads/'+$(this).parent().data('permalink');
	});
	
	$('.edit-iklan .delete').click(function(){
		item_id = $(this).parent().data('item_id')
		$.ajax({
			url: $('body').data('base_url')+'ajax/delete_item/',
			dataType: 'json',
			data: {item_id: item_id, permalink: $(this).parent().data('permalink')},
			type: 'post'
		})
		.success(function(data){
			if(data.status == true){
				$("[data-item_id='"+item_id+"'").parents('li').remove();
				custom_alert(true, 'Barang berhasil dihapus');
			}
			else if(data.status == false) {
				custom_alert(false, 'Terjadi kesalahan, silahkan ulangi');
			}
		});
	});

	$('.address-wrap input').change(function(){
		string = '';
		arrInput = ['alamat', 'rt', 'rw', 'kelurahan', 'kecamatan', 'kode_pos'];
		for(index = 0; index < arrInput.length; index++) {
			if($('#'+arrInput[index]).val() != '') {
				string += (index !=0 ) ? arrInput[index]+': ' : '';
				string += $('#'+arrInput[index]).val()+'<br/>';
			}
		}
		$('#address').val(string);
		console.log($('#address').val());
	});

	$('#address-trigger').click(function(e){
		e.preventDefault();
		$('#hidden-input').css('display', 'block');
		$('#hidden-address').css('display', 'none');
	});
	

});
