// 1 - START DROPDOWN SLIDER SCRIPTS ------------------------------------------------------------------------

$(document).ready(function () {
    $(".showhide-account").click(function () {
        $(".account-content").slideToggle("fast");
        $(this).toggleClass("active");
        return false;
    });
});

$(document).ready(function () {
    $(".action-slider").click(function () {
        $("#actions-box-slider").slideToggle("fast");
        $(this).toggleClass("activated");
        return false;
    });
});

//  END ----------------------------- 1

// 2 - START LOGIN PAGE SHOW HIDE BETWEEN LOGIN AND FORGOT PASSWORD BOXES--------------------------------------

$(document).ready(function () {
	$(".forgot-pwd").click(function () {
	$("#loginbox").hide();
	$("#forgotbox").show();
	return false;
	});

});

$(document).ready(function () {
	$(".back-login").click(function () {
	$("#loginbox").show();
	$("#forgotbox").hide();
	return false;
	});
});

// END ----------------------------- 2



// 3 - MESSAGE BOX FADING SCRIPTS ---------------------------------------------------------------------

$(document).ready(function() {
	$(".close-yellow").click(function () {
		$("#message-yellow").fadeOut("slow");
	});
	$(".close-red").click(function () {
		$("#message-red").fadeOut("slow");
	});
	$(".close-blue").click(function () {
		$("#message-blue").fadeOut("slow");
	});
	$(".close-green").click(function () {
		$("#message-green").fadeOut("slow");
	});
});

// END ----------------------------- 3



// 4 - CLOSE OPEN SLIDERS BY CLICKING ELSEWHERE ON PAGE -------------------------------------------------------------------------

$(document).bind("click", function (e) {
    if (e.target.id != $(".showhide-account").attr("class")) $(".account-content").slideUp();
});

$(document).bind("click", function (e) {
    if (e.target.id != $(".action-slider").attr("class")) $("#actions-box-slider").slideUp();
});
// END ----------------------------- 4
 
 
 
// 5 - TABLE ROW BACKGROUND COLOR CHANGES ON ROLLOVER -----------------------------------------------------------------------
/*
$(document).ready(function () {
    $('#product-table	tr').hover(function () {
        $(this).addClass('activity-blue');
    },
    function () {
        $(this).removeClass('activity-blue');
    });
});
 */
// END -----------------------------  5
 
 
 
 // 6 - DYNAMIC YEAR STAMP FOR FOOTER -----------------------------------------------------------------------

 $('#spanYear').html(new Date().getFullYear()); 
 
// END -----------------------------  6 

$(document).ready(function(){
	$('.payment_action').click(function(e){
		e.preventDefault();
		var payment_approval;
		var new_action
		var new_status
		item_id = $(this).attr('item_id');
		if($(this).html() == 'Setujui') {
			payment_approval = 1;
			new_action = 'Batalkan';
			new_status = 'Telah disetujui';
		}
		else {
			payment_approval = 0;
			new_action = 'Setujui';
			new_status = 'Belum disetujui';
		}
		$.ajax({
			url: $('body').attr('base_url')+'ajax/payment_action',
			dataType: 'json',
			data: {item_id: item_id, booking_token: $(this).attr('booking_token'), payment_approval: payment_approval},
			type: 'post'
		})
		.success(function(data){
			console.log(data.status);
			if(data.status == true) {
				custom_alert('green', data.message);
				$('.payment_'+item_id).html(new_action);
				$('.status_'+item_id).html(new_status);
			}
			else if(data.status == false) {
				custom_alert('red', data.message);
			}
		});
	});

	$(".delete_item").click(function(e){
		e.preventDefault();
		item_id = $(this).data('item_id');

		$.ajax({
			url: $('body').attr('base_url')+'ajax/delete_item',
			dataType: 'json',
			data: {item_id: item_id},
			type: 'post'
		})
		.success(function(data){
			if(data.status == true) {
				$('.item_'+item_id).parents('tr.item_'+item_id).remove();
				custom_alert('green', data.message);
			}
			else if(data.status == false) {
				custom_alert('red', data.message);
			}
		});
	});

	function custom_alert(type, message){
		$('#message-'+type).attr('style', 'display: block;');
		$('#message-'+type+' td.'+type+'-left').html(message);
	}

	$('.delete_user').click(function(e){
		e.preventDefault();
		user_id = $(this).data('user_id');

		$.ajax({
			url: $('body').attr('base_url')+'ajax/delete_user',
			dataType: 'json',
			data: {user_id: user_id},
			type: 'post'
		})
		.success(function(data){
			if(data.status == true) {
				$('.user_'+user_id).parents('tr.user_'+user_id).remove();
				custom_alert('green', data.message);
			}
			else if(data.status == false) {
				custom_alert('red', data.message);
			}
		});
	});
});
  
