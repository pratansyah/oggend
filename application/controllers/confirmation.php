<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Confirmation extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->model('transaction_mdl');
		$this->load->config('values');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->library('Image');
		$this->load->library('Mail');

		$this->image_helper = new Image();
		$this->Mail = new Mail();

		if(!$this->session->userdata('username')) {
			redirect('/signup');
		}
	}

	function index() {
		redirect('dashboard');
	}

	function payment() {
		if($this->input->post()) {
			$this->form_validation->set_rules('token', 'Token', 'required');
			if ($this->form_validation->run() == TRUE && !empty($_FILES['trans_pic'])){
				extract($this->input->post());
				$payment_token = $this->transaction_mdl->confirm_token(array('payment_token' => $token));
				if($payment_token) {
					$config['upload_path'] = $this->config->item('confirmation_real_path')."/payment/";
					$config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
					$config['max_size'] = '100';
					$config['max_width']  = '2000';
					$config['max_height']  = '2000';
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload');
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('trans_pic')) {
						$this->session->set_flashdata(array('error_message' => $this->upload->display_errors()));
					}
					else {
						$data = $this->upload->data();
						// $trans['payment_image'] = $data['file_name'];
						// $trans['payment_token'] = $token;
						$obj_update = new stdClass();
						$obj_update->payment_image = $data['file_name'];
						$obj_update->sent_token = md5($data['file_name'].$this->session->userdata('user_id').time());

						$param['buyer_id'] = $this->session->userdata('user_id');
						$param['payment_token'] = $token;

						$update = $this->transaction_mdl->update_trans($obj_update, $param);
						if($update) {
							$transaction = $this->transaction_mdl->get_transaction_detail($param);
							$buyer = $this->user_mdl->get_user_by_user_id($this->session->userdata('user_id'));
							$item = $this->item_mdl->get_item_detail_by_id($transaction->item_id);
							$subject = 'Pembayaran barang';
							$message = '<a href="http://jualbeliyuk.com/user/'.$buyer->username.'">'.$buyer->username.'</a> telah membayar <a href="http://jualbeliyuk.com/ads/detail/'.$item->permalink.'">'.$item->title.'</a>';
							if($this->Mail->send_email($subject, $message)) {
								$this->session->set_flashdata(array('error_message' => 'Pembayaran akan segera dievaluasi'));
							}
							else {
								$this->session->set_flashdata(array('error_message' => 'Silahkan ulangi proses konfirmasi'));
							}
						}
						else {
						}

						redirect('dashboard/history/buy');
					}
				}
				else {
					$this->session->set_flashdata(array('error_message' => 'Token tidak valid'));
				}

			}
			else {
				$this->session->set_flashdata(array('error_message' => 'Silahkan lengkapi data'));
			}
			
			redirect('dashboard/confirmation/payment/');

		}
		$this->data['content'] = 'dashboard/payment';
		$this->load->view('dashboard/main', $this->data);
	}


	function send() {
		if($this->input->post()) {
			$this->form_validation->set_rules('token', 'Token', 'required');
			if ($this->form_validation->run() == TRUE && !empty($_FILES['send_pic'])){
				extract($this->input->post());
				$sent_token = $this->transaction_mdl->confirm_token(array('sent_token' => $token));
				if($sent_token) {
					$config['upload_path'] = $this->config->item('confirmation_real_path')."/sent/";
					$config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
					$config['max_size'] = '100';
					$config['max_width']  = '2000';
					$config['max_height']  = '2000';
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload');
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('send_pic')) {
						$this->session->set_flashdata(array('error_message' => $this->upload->display_errors()));
					}
					else {
						$data = $this->upload->data();
						$obj_update = new stdClass();
						$obj_update->sent_image = $data['file_name'];

						$param['seller_id'] = $this->session->userdata('user_id');
						$param['sent_token'] = $token;
						$update = $this->transaction_mdl->update_trans($obj_update, $param);
						if($update) {
							$transaction = $this->transaction_mdl->get_transaction_detail($param);
							$seller = $this->user_mdl->get_user_by_user_id($this->session->userdata('user_id'));
							$item = $this->item_mdl->get_item_detail_by_id($transaction->item_id);
							$subject = 'Pengiriman barang';
							$message = '<a href="http://jualbeliyuk.com/user/'.$seller->username.'">'.$seller->username+'</a> telah mengirim <a href="http://jualbeliyuk.com/ads/detail/'.$item->permalink.'">'+$item->title.'</a>';
							if($this->Mail->send_email($subject, $message)) {
								$this->session->set_flashdata(array('error_message' => 'Pengiriman akan segera dievaluasi'));
							}
							else {
								$this->session->set_flashdata(array('error_message' => 'Silahkan ulangi proses konfirmasi'));
							}
						}
						else {
							$this->session->set_flashdata(array('error_message' => 'Silahkan ulangi proses konfirmasi'));
						}

						redirect('dashboard/history/sell');
					}
				}
				else {
					$this->session->set_flashdata(array('error_message' => 'Token tidak valid'));
				}

			}
			else {
				$this->session->set_flashdata(array('error_message' => 'Silahkan lengkapi data'));
			}
			
			redirect('dashboard/confirmation/send/');

		}
		$this->data['content'] = 'dashboard/send';
		$this->load->view('dashboard/main', $this->data);
	}

	function arrival() {
		if($this->input->post()) {
			$this->form_validation->set_rules('token', 'Token', 'required');
			$this->form_validation->set_rules('rating', 'Rating', 'required');
			if ($this->form_validation->run() == TRUE){
				extract($this->input->post());
				$payment_token = $this->transaction_mdl->confirm_token(array('payment_token' => $token));
				if($payment_token) {
					switch ($rating) {
						case '1':
							$rating = 20;
							break;
						case '2':
							$rating = 40;
							break;
						case '3':
							$rating = 60;
							break;
						case '4':
							$rating = 80;
							break;
						case '5':
							$rating = 100;
							break;
						
						default:
							$rating = 50;
							break;
					}
					$obj_update_trans = new stdClass();

					$obj_update_trans->review_value = $rating;
					$obj_update_trans->arrival_status = 1;

					$param_trans['buyer_id'] = $this->session->userdata('user_id');
					$param_trans['payment_token'] = $token;
					$update_trans = $this->transaction_mdl->update_trans($obj_update_trans, $param_trans);

					$trans_obj = $this->transaction_mdl->get_transaction_detail($param_trans);

					$update_item = $this->item_mdl->update_item(array('active' => 'f'), $trans_obj->item_id);
					if($update_trans && $update_item) {
						$buyer = $this->user_mdl->get_user_by_user_id($this->session->userdata('user_id'));
						$item = $this->item_mdl->get_item_detail_by_id($trans_obj->item_id);
						$subject = 'Barang telah sampai';
						$message = '<a href="http://jualbeliyuk.com/user/'.$buyer->username.'">'.$buyer->username.'</a> telah menerima <a href="http://jualbeliyuk.com/ads/detail/'.$item->permalink.'">'+$item->title.'</a>';
						if($this->Mail->send_email($subject, $message)) {
							$this->session->set_flashdata(array('error_message' => 'Terima kasih'));
						}
						else {
							$this->session->set_flashdata(array('error_message' => 'Silahkan ulangi proses konfirmasi'));
						}
					}
					else {
						$this->session->set_flashdata(array('error_message' => 'Silahkan ulangi proses konfirmasi'));
					}

					redirect('dashboard/history/buy');
				}
				else {
					$this->session->set_flashdata(array('error_message' => 'Token tidak valid'));
				}

			}
			else {
				$this->session->set_flashdata(array('error_message' => 'Silahkan lengkapi data'));
			}
			
			redirect('dashboard/confirmation/arrival/');

		}
		$this->data['content'] = 'dashboard/arrival';
		$this->load->view('dashboard/main', $this->data);
	}
}