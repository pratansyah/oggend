<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Misc extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->library('Mail');

		$this->Mail = new Mail();
	}

	function index() {
		redirect('');
	}

	function contact() {
		if($this->input->post()){
			extract($this->input->post());
			$to = 'contact@jualbeliyuk.com';
			$subject = 'Kritik & Saran';
			$message .= "Nama:".$name."<br>";
			$message .= "Email:".$email."<br><br>";
			$message .= $message;
			if($this->Mail->send_email($subject, $message, $to)) {
				$this->session->set_flashdata(array('error_message' => 'Pesan telah disampaikan'));
			}
		}
		$this->data['content'] = 'misc/contact';
		$this->load->view('main', $this->data);
	}

	function terms() {
		$this->data['content'] = 'misc/terms';
		$this->load->view('main', $this->data);
	}

	function how() {
		$this->data['content'] = 'misc/how';
		$this->load->view('main', $this->data);
	}

	function apa_jby() {
		$this->data['content'] = 'misc/apa_jby';
		$this->load->view('main', $this->data);
	}
}