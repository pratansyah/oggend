<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	private $_per_page = 10;
	private $_offset = 0;
	private $_current_page = 1;

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->model('transaction_mdl');

		if(!$this->session->userdata('admin')) {
			redirect('/admin');
		}

		if(isset($_GET['page']) && $_GET['page']>0) {
			$this->_offset = ($_GET['page']-1) * $this->_per_page;
			$this->_current_page = $_GET['page'];
		}
		
		if(!$this->session->userdata('admin')) {
			redirect('admin/login');
		}
	}

	function index() {
		redirect('admin/users/all');
	}

	function all() {
		$this->data['method'] .= '_all';
		$total_rows = 0;
		$this->data['users'] = $this->user_mdl->get_users('', $order="", $sort="", $this->_per_page, $this->_offset, $total_rows);
		$this->data['content'] = 'admin/users';
		$pagination['total_pages'] = (int)ceil($total_rows/$this->_per_page);
		$pagination['current_page'] = $this->_current_page;
		$this->data['pagination'] = $pagination;
		$this->load->view('admin/main', $this->data);
	}

}