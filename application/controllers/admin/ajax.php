<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('transaction_mdl');
		$this->load->model('item_mdl');
		$this->load->model('image_mdl');
		$this->load->library('Mail');

		$this->Mail = new Mail();
	}

	function index() {
		echo '';
	}

	function payment_action() {
		if($this->input->post() && $this->session->userdata('admin') === TRUE) {
			extract($this->input->post());
			$param['item_id'] = $item_id;
			$param['booking_token'] = $booking_token;

			$obj_update['payment_approval'] = ($payment_approval == 1) ? $payment_approval : 0;

			$update = $this->transaction_mdl->update_trans($obj_update, $param);
			if($update) {
				if($obj_update['payment_approval'] == 1) {
					$trans = $this->transaction_mdl->get_transaction_detail($param);
					$detail = $this->transaction_mdl->get_buyer_detail($trans->buyer_id, $trans->item_id);
					$seller = $this->user_mdl->get_user_by_user_id($trans->seller_id);

					$detail = $detail[0];
					$subject = "Barang anda telah terjual!";
					$message = "<a href='".base_url()."ads/detail/".$detail->permalink."'>".$detail->title."</a> telah terjual kepada pembeli dengan detail sebagai berikut:<br>";
					$message .= "Nama:".$detail->fullname."<br>";
					$message .= "Alamat:".$detail->address."<br>";
					$message .= "Provinsi/Kota:".$detail->location_name."<br><br>";
					$message .= "Silahkan kirim barang yang dijual ke alamat tersebut dan konfirmasikan pengiriman di dashboard. Uang akan kami transferkan ke rekening anda setelah pembeli mengkonfirmasi kedatangan barang";
					$to = $seller->email;
					$this->Mail->send_email($subject, $message, $to);
				}

				$result = array('status' => TRUE, 'message' => 'Update berhasil');
			}
			else {
				$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		else {
			$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi');
		}

		echo json_encode($result);
	}

	function delete_item() {
		if($this->input->post() && $this->session->userdata('admin') === TRUE) {
			extract($this->input->post());
			$param['item_id'] = $item_id;

			$images = $this->image_mdl->get_images($item_id);
			foreach ($images as $image) {
				unlink(realpath(APPPATH . '../images/product/big/'.$image->image));
				unlink(realpath(APPPATH . '../images/product/small/'.$image->image));
				unlink(realpath(APPPATH . '../images/product/thumb/'.$image->image));

				$this->image_mdl->delete_image(array('image_id' => $image->image_id, 'item_id' => $item_id));
			}
			$delete_item = $this->item_mdl->delete_item($param);
			$delete_transaction = $this->transaction_mdl->delete_transaction($param);
			if($delete_item && $delete_transaction) {
				$result = array('status' => TRUE, 'message' => 'Berhasil dihapus');
			}
			else {
				$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		else {
			$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi');
		}

		echo json_encode($result);
	}

	function delete_user() {
		if($this->input->post() && $this->session->userdata('admin') === TRUE) {
			extract($this->input->post());
			$param['user_id'] = $user_id;
			$items = $this->item_mdl->get_item_ids($user_id);

			$delete_user  = $this->user_mdl->delete_user($param);
			$delete_item = TRUE;
			$delete_transaction = TRUE;

			if($items) {
				foreach($items as $item_id) {
					$images = $this->image_mdl->get_images($item_id);
					if($images) {
						foreach ($images as $image) {
							unlink(realpath(APPPATH . '../images/product/big/'.$image->image));
							unlink(realpath(APPPATH . '../images/product/small/'.$image->image));
							unlink(realpath(APPPATH . '../images/product/thumb/'.$image->image));

							$this->image_mdl->delete_image(array('image_id' => $image->image_id, 'item_id' => $item_id));
						}
					}
					$delete_item = $this->item_mdl->delete_item($param);
					$delete_transaction = $this->transaction_mdl->delete_transaction(array('seller_id' => $user_id));
				}
			}

			if($delete_item && $delete_transaction && $delete_user) {
				$result = array('status' => TRUE, 'message' => 'Pengguna berhasil dihapus');
			}
			else {
				$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		else {
			$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi lagi');
		}

		echo json_encode($result);
	}
}