<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions extends MY_Controller {

	private $_per_page = 10;
	private $_offset = 0;
	private $_current_page = 1;

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->model('transaction_mdl');

		if(!$this->session->userdata('admin')) {
			redirect('/admin');
		}

		if(isset($_GET['page']) && $_GET['page']>0) {
			$this->_offset = ($_GET['page']-1) * $this->_per_page;
			$this->_current_page = $_GET['page'];
		}
		if(!$this->session->userdata('admin')) {
			redirect('admin/login');
		}
	}

	function index() {
		redirect('admin/transactions/all');
	}

	function all() {
		$this->data['method'] .= '_all';
		$total_rows = 0;
		$this->data['transactions'] = $this->transaction_mdl->get_transactions("", "booked_time", "DESC", $this->_per_page, $this->_offset, $total_rows);
		$this->data['content'] = 'admin/transactions';
		$pagination['total_pages'] = (int)ceil($total_rows/$this->_per_page);
		$pagination['current_page'] = $this->_current_page;
		$this->data['pagination'] = $pagination;
		$this->load->view('admin/main', $this->data);
	}

	function payment() {
		$this->data['method'] .= '_payment';
		$total_rows = 0;
		$param = array('payment_image !=' => "", 'sent_token !=' => "");
		$this->data['transactions'] = $this->transaction_mdl->get_transactions($param, "booked_time", "DESC", $this->_per_page, $this->_offset, $total_rows);
		$this->data['content'] = 'admin/payment';
		$pagination['total_pages'] = (int)ceil($total_rows/$this->_per_page);
		$pagination['current_page'] = $this->_current_page;
		$this->data['pagination'] = $pagination;
		$this->load->view('admin/main', $this->data);
	}

	function sent() {
		$this->data['method'] .= '_sent';
		$total_rows = 0;
		$param = array('sent_image !=' => '', 'payment_approval' => 1);
		$this->data['transactions'] = $this->transaction_mdl->get_transactions($param, "booked_time", "DESC", $this->_per_page, $this->_offset, $total_rows);
		$this->data['content'] = 'admin/sent';
		$pagination['total_pages'] = (int)ceil($total_rows/$this->_per_page);
		$pagination['current_page'] = $this->_current_page;
		$this->data['pagination'] = $pagination;
		$this->load->view('admin/main', $this->data);
	}

}