<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('transaction_mdl');
	}

	function index() {
		redirect('');
	}

	public function delete_outdated_transaction() {
		$this->transaction_mdl->cron_terminate_booking();
	}

}