<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->model('transaction_mdl');
		$this->load->library('Mail');

		$this->Mail = new Mail();
	}

	function index() {
		echo '';
	}

	function check_username($username=false) {
		if($username){
			$result = array();
			$username = $this->user_mdl->check_username($username);
			if(!$username) {
				$result = array('status' => true, 'string' => 'Username bisa digunakan');
			}
			else {
				$result = array('status' => false, 'string' => 'Username telah dipakai, silahkan pilih username yang lain');
			}
			echo json_encode($result);
		}
	}

	function load_city($province_id=false) {
		if($province_id) {
			$result = $this->location_mdl->get_cities($province_id);
			if($result) {
				echo json_encode(array('status' => true, 'cities' => $result));
			}
			else {
				echo json_encode(array('status' => false));
			}
		}
	}

	function load_child_categories($category_id = false) {
		if($category_id) {
			echo json_encode($this->category_mdl->get_categories($category_id));
		}
	}

	function book() {
		if($this->session->userdata('user_id') && $this->input->post()) {
			extract($this->input->post());
			if($token && $item_id) {
				$seller_id = $this->item_mdl->get_user_id($item_id);
				if($seller_id == $this->session->userdata('user_id')){
					echo json_encode(array('status' => 'same_user'));die();
				}
				$result = $this->item_mdl->book_item($item_id, $token, $this->session->userdata('user_id'));
				if($result) {
					$transaction->seller_id = $seller_id;
					$transaction->buyer_id = $this->session->userdata('user_id');
					$transaction->booking_token = $token;
					$transaction->item_id = $item_id;
					$transaction->payment_token = md5($transaction->seller_id.time().$transaction->buyer_id);

					$trans_result = $this->transaction_mdl->add_transaction($transaction);

					$buyer = $this->user_mdl->get_user_by_user_id($this->session->userdata('user_id'));
					$item = $this->item_mdl->get_item_detail_by_id($transaction->item_id);
					$subject = 'Pemesanan barang';
					$message = '<a href="http://jualbeliyuk.com/user/'.$buyer->username.'">'.$buyer->username.'</a> telah memesan <a href="http://jualbeliyuk.com/ads/detail/'.$item->permalink.'">'.$item->title.'</a><br/>Email pembeli: '.$buyer->email;

					if($this->Mail->send_email($subject, $message)) {
						$this->session->set_flashdata(array('error_message' => 'Pembayaran akan segera dievaluasi'));
					}
					else {
						$this->session->set_flashdata(array('error_message' => 'Silahkan ulangi proses konfirmasi'));
					}
					echo json_encode(array('status' => $trans_result));
				}
				else {
					echo json_encode(array('status' => $result));
				}
			}
		}
		else {
			echo json_encode(array('status' => 'not_login'));
		}
	}

	function delete_item() {
		if($this->session->userdata('user_id') && $this->input->post()) {
			extract($this->input->post());
			$param['item_id'] = $item_id;
			$param['user_id'] = $this->session->userdata('user_id');

			$images = $this->image_mdl->get_images($item_id);
			foreach ($images as $image) {
				unlink(realpath(APPPATH . '../images/product/big/'.$image->image));
				unlink(realpath(APPPATH . '../images/product/small/'.$image->image));
				unlink(realpath(APPPATH . '../images/product/thumb/'.$image->image));

				$this->image_mdl->delete_image(array('image_id' => $image->image_id, 'item_id' => $item_id));
			}
			$delete_item = $this->item_mdl->delete_item($param);
			$delete_transaction = $this->transaction_mdl->delete_transaction(array('seller_id' => $param['user_id'], 'item_id' => $item_id));
			if($delete_item && $delete_transaction) {
				$result = array('status' => TRUE, 'message' => 'Berhasil dihapus');
			}
			else {
				$result = array('status' => FALSE, 'message' => 'Terjadi kesalahan, silahkan ulangi');
			}
			echo json_encode($result);
		}
	}
}