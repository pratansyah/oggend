<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ads extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
	}

	function index() {
		redirect('');
	}

	function detail($permalink=false) {
		if(!$permalink) {
			redirect('');
		}
		$this->data['detail'] = $this->item_mdl->get_item_detail($permalink);
		$this->data['same_category'] = $this->item_mdl->get_newest_items_by_category($this->data['detail']['item']->category_id);
		$this->data['category'] = $this->category_mdl->get_category_id_by_slug($this->data['detail']['item']->category_slug);
		$this->data['content'] = 'ads_detail';
		$this->load->view('main', $this->data);
	}
}