<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class History extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('user_mdl');
		$this->load->model('transaction_mdl');
		
		if(!$this->session->userdata('username')) {
			redirect('/signup');
		}

	}

	public function buy() {
		$this->data['items'] = $this->transaction_mdl->get_bought_items($this->session->userdata('user_id'));
		// var_dump($this->data['items']);die();
		$this->data['content'] = 'dashboard/history_buy';
		$this->load->view('dashboard/main', $this->data);
	}

	public function sell() {
		$this->data['items'] = $this->transaction_mdl->get_sold_items($this->session->userdata('user_id'));
		// var_dump($this->data['items']);die();
		$this->data['content'] = 'dashboard/history_sell';
		$this->load->view('dashboard/main', $this->data);
	}
}

?>