<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends MY_Controller {

	private $_username_unique = TRUE;
	private $_email_unique = TRUE;
	private $_valid_email = TRUE;
	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->config('values');
		$this->load->library('form_validation');
		$this->load->library('Mail');

		$this->Mail = new Mail();
	}

	public function index() {
		$this->data['content'] = 'signup';
		if($this->input->post()) {
			$this->form_validation->set_rules('username', 'Username', 'callback_username_check');
			$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('email', 'Email', 'callback_email_check');
			$this->form_validation->set_rules('rgroup', 'Jenis Kelamin', 'required');
			$this->form_validation->set_rules('no_hp', 'Nomor Telepon', 'required');
			if ($this->form_validation->run() == TRUE) {
				extract($this->input->post());
				$user['fullname'] = $full_name;
				$user['password'] = md5($password.$username.$this->config->item('user_password_salt'));
				$user['username'] = $username;
				$user['avatar'] = 'default.png';
				$user['email'] = $email;
				$user['sex'] = $rgroup;
				$user['created'] = date('Y-m-d');
				$user['contact'] = $no_hp;
				$user['location_id'] = 0;
				$result = $this->user_mdl->create_user($user);
				if($result !== false){
					$this->session->set_userdata(array('username' => $username, 'user_id' => $result));
					$subject = "Registrasi jualbeliyuk.com";
					$message = "Selamat! Email anda telah terdaftar di jualbeliyuk.com silahkan login dan isi data diri anda selengkap mungkin";
					$to = $email;
					$this->Mail->send_email($subject, $message, $to);
					redirect('/dashboard/profile');
				}
			}
			elseif(!$this->_username_unique) {
				$this->session->set_flashdata('error_message', 'Username telah dipakai, silahkan pilih username yang lain');
				redirect('signup');
			}
			elseif(!$this->_email_unique) {
				$this->session->set_flashdata('error_message', 'Email telah dipakai, silahkan pilih email yang lain');
				redirect('signup');
			}
			elseif(!$this->_valid_email) {
				$this->session->set_flashdata('error_message', 'Alamat email tidak valid, silahkan pilih email yang lain');
				redirect('signup');
			}
			else {
				$this->session->set_flashdata('error_message', validation_errors());
				redirect('signup');
			}
		}
		$this->load->view('main', $this->data);
	}

	public function username_check($str) {
		$username = $this->user_mdl->check_username($str);
		if(!$username) {
			return TRUE;
		}
		else {
			$this->_username_unique = FALSE;
			return FALSE;
		}
	}

	public function email_check($str) {
		$valid = preg_match('~[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}~', $str);
		if($valid) {
			$email = $this->user_mdl->check_email($str);
			if(!$email) {
				return TRUE;
			}
			else {
				$this->_email_unique = FALSE;
				return FALSE;
			}
		}
		else {
			$this->_valid_email = FALSE;
			return FALSE;
		}

	}
}