<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	private $image_helper;
	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->model('image_mdl');
		$this->load->config('values');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->library('Image');

		$this->image_helper = new Image();

		if(!$this->session->userdata('username')) {
			redirect('/signup');
		}
	}

	public function index() {
		redirect('/dashboard/profile');
	}

	public function profile() {
		if($this->input->post()) {

			$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('rgroup', 'Jenis Kelamin', 'required');
			$this->form_validation->set_rules('no_hp', 'Nomor Telepon', 'required');
			$this->form_validation->set_rules('bank', 'Nama Bank', 'required');
			$this->form_validation->set_rules('no_rek', 'No. Rekening', 'required');
			$this->form_validation->set_rules('address', 'Alamat', 'required');
			if($this->form_validation->run() == TRUE) {
				extract($this->input->post());
				$user = new stdClass();
				$user->fullname = $full_name;
				if (!empty($password)) $user->password = md5($password.$this->session->userdata('username').$this->config->item('user_password_salt'));
				$user->email = $email;
				if (!empty($kabupaten)) $user->location_id = $kabupaten; elseif(!empty($province_location)) $user->location_id = $province_location;
				$user->bank_name = $bank;
				$user->account_number = $no_rek;
				$user->sex = $rgroup;
				$user->address = $address;
				$user->contact = $no_hp;

				if (!empty($_FILES['ava_pic']['size'])) {
					$current_ava = $this->user_mdl->get_avatar_filename($this->session->userdata('username'));
					$config['upload_path'] = $this->config->item('avatar_real_path');
					$config['allowed_types'] = 'gif|jpg|jpeg|bmp';
					$config['max_size'] = '200';
					$config['max_width']  = '1000';
					$config['max_height']  = '1000';
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload');
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('ava_pic')) {
						$this->session->set_flashdata(array('error_message' => $this->upload->display_errors()));
						redirect('/dashboard/profile');
					}
					else {
						$data = $this->upload->data();
						$user->avatar = $data['file_name'];
						if($current_ava->avatar != 'default.png') unlink($this->config->item('avatar_real_path')."/".$current_ava->avatar);
					}
				}
				$this->user_mdl->update_user($user);

			}
			else {
				$this->session->set_flashdata(array('error_message' => 'Silahkan lengkapi data'));
				redirect('/dashboard/profile');

			}
		}
		$this->data['user'] = $this->user_mdl->get_user_by_username($this->session->userdata('username'));
		$this->data['content'] = 'dashboard/profile';
		$this->load->view('dashboard/main', $this->data);
	}

	public function my_ads() {
		$this->data['items'] = $this->item_mdl->get_items($this->session->userdata('user_id'));
		$this->data['content'] = 'dashboard/my_ads';
		$this->load->view('dashboard/main', $this->data);
	}

	public function create_ads() {
		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'Judul', 'required');
			$this->form_validation->set_rules('harga', 'Harga', 'required');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
			$this->form_validation->set_rules('kategori', 'Kategori', 'required');
			$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
			$this->form_validation->set_rules('shipped', 'Dikirim', 'required');
			$this->form_validation->set_rules('kondisi', 'Kondisi', 'required');
			if ($this->form_validation->run() == TRUE){
				extract($this->input->post());
				$item = new stdClass();
				$item->user_id = $this->session->userdata('user_id');
				$item->active = 't';
				$item->permalink = preg_replace('/[\-]+/', '-', preg_replace('/[^a-zA-Z0-9]/', '-', trim(strtolower($title), 35))).'-'.time();
				$item->title = $title;
				$item->booking_token = md5($item->permalink);
				$item->price = $harga;
				$item->description = $deskripsi;
				$item->category_id = $kategori;
				$item->location_id = $lokasi;
				$item->shipped = ('ya' == strtolower($shipped)) ? 1 : 0;
				$item->condition = ('baru' == strtolower($kondisi)) ? 1 : 0;
				$item_id = $this->item_mdl->create_item($item);
				if($item_id !== FALSE) {
					if (!empty($_FILES['pic'])) {
						$config['upload_path'] = $this->config->item('product_image_real_path').'/big/';
						$config['allowed_types'] = 'gif|jpg|jpeg|bmp';
						$config['max_size'] = '100';
						$config['max_width']  = '2000';
						$config['max_height']  = '2000';
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload');
						$this->upload->initialize($config);

						//Deleting empty files
						foreach($_FILES['pic']['name'] as $key => $value) {
							if($value == '') $empty_keys[] = $key;
						}
						$delete_keys = array('name', 'type', 'tmp_name', 'error', 'size');
						foreach($delete_keys as $delete_key) {
							foreach($empty_keys as $empty_key) {
								unset($_FILES['pic'][$delete_key][$empty_key]);
							}
						}
						if ( ! $this->upload->do_multi_upload('pic')) {
							$this->session->set_flashdata(array('error_message' => $this->upload->display_errors()));
						}
						else {
							$multi_data = $this->upload->get_multi_upload_data();
							foreach($multi_data as $data) {
								$image['image'] = $data['file_name'];
								$image['item_id'] = $item_id;
								$this->image_helper->openImage($this->config->item('product_image_real_path').'/big/'.$image['image']);
								$this->image_helper->resizeImage(230, 150, 'crop');
								$this->image_helper->saveImage($this->config->item('product_image_real_path').'/thumb/'.$image['image']);
								$this->image_helper->resizeImage(480, 320, 'crop');
								$this->image_helper->saveImage($this->config->item('product_image_real_path').'/small/'.$image['image']);
								$this->image_mdl->add_image($image);							
							}
						}
					}
					redirect('/dashboard/my_ads');
				}
			}
		}
		$this->data['content'] = 'dashboard/create_ads_2';
		$this->load->view('main', $this->data);
	}

	public function edit_ads($permalink) {
		$item = $this->item_mdl->get_item_by_permalink($permalink, $this->session->userdata('user_id'));
		if($item) {
			$this->session->set_userdata(array('item_id' => $item->item_id));
			$this->data['item'] = $item;
			if($this->input->post()) {
				$this->form_validation->set_rules('title', 'Judul', 'required');
				$this->form_validation->set_rules('harga', 'Harga', 'required');
				$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
				$this->form_validation->set_rules('kategori', 'Kategori', 'required');
				$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
				$this->form_validation->set_rules('shipped', 'Dikirim', 'required');
				$this->form_validation->set_rules('kondisi', 'Kondisi', 'required');
				if ($this->form_validation->run() == TRUE){
					extract($this->input->post());
					$edited_item = new stdClass();
					// $edited_item->permalink = preg_replace('/[\-]+/', '-', preg_replace('/[^a-zA-Z0-9]/', '-', trim(strtolower($title), 35))).'-'.time();
					$edited_item->title = $title;
					$edited_item->price = $harga;
					$edited_item->description = $deskripsi;
					$edited_item->category_id = $kategori;
					$edited_item->location_id = $lokasi;
					$edited_item->shipped = ('ya' == strtolower($shipped)) ? 1 : 0;
					$edited_item->condition = ('baru' == strtolower($kondisi)) ? 1 : 0;

					$param['user_id'] = $this->session->userdata('user_id');
					$param['item_id'] = $this->session->userdata('item_id');
					$result = $this->item_mdl->edit_item($edited_item, $param);
					if($result) {
						$posted_images = array_filter($_FILES['pic']['size']);
						if (!empty($posted_images)) {
							$config['upload_path'] = $this->config->item('product_image_real_path').'/big/';
							$config['allowed_types'] = 'gif|jpg|jpeg|bmp';
							$config['max_size'] = '100';
							$config['max_width']  = '2000';
							$config['max_height']  = '2000';
							$config['encrypt_name'] = TRUE;
							$this->load->library('upload');
							$this->upload->initialize($config);

							$images = $this->image_mdl->get_images($this->session->userdata('item_id'));
							foreach($_FILES['pic']['name'] as $key => $value) {
								if($value == '') $empty_keys[] = $key;
							}
							$delete_keys = array('name', 'type', 'tmp_name', 'error', 'size');
							foreach($delete_keys as $delete_key) {
								foreach($empty_keys as $empty_key) {
									unset($_FILES['pic'][$delete_key][$empty_key]);
								}
							}
							if ( ! $this->upload->do_multi_upload('pic')) {
								$this->session->set_flashdata(array('error_message' => $this->upload->display_errors()));
							}
							else {
								$multi_data = $this->upload->get_multi_upload_data();
								foreach($multi_data as $data) {
									$new_image['image'] = $data['file_name'];
									$new_image['item_id'] = $this->session->userdata('item_id');
									$this->image_helper->openImage($this->config->item('product_image_real_path').'/big/'.$new_image['image']);
									$this->image_helper->resizeImage(230, 150, 'crop');
									$this->image_helper->saveImage($this->config->item('product_image_real_path').'/thumb/'.$new_image['image']);
									$this->image_helper->resizeImage(480, 320, 'crop');
									$this->image_helper->saveImage($this->config->item('product_image_real_path').'/small/'.$new_image['image']);
									$this->image_mdl->add_image($new_image);							
								}
								foreach($images as $image) {
									unlink(realpath(APPPATH . '../images/product/big/'.$image->image));
									unlink(realpath(APPPATH . '../images/product/small/'.$image->image));
									unlink(realpath(APPPATH . '../images/product/thumb/'.$image->image));

									$this->image_mdl->delete_image(array('image_id' => $image->image_id, 'item_id' => $this->session->userdata('item_id')));
								}
								//Deleting empty files
							}
						}
						$this->session->set_flashdata(array('error_message' => 'Pembaharuan produk berhasil'));
						redirect('/dashboard/my_ads');
					}
					else {
						$this->session->set_flashdata(array('error_message' => 'Terjadi kesalahan, silahkan ulangi'));
						redirect('/dashboard/my_ads');
					}
				}
			}
			$this->data['content'] = 'dashboard/edit_ads';
		$this->load->view('main', $this->data);
		}
		else {
			redirect('/dashboard/my_ads');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('');
	}
}

?>