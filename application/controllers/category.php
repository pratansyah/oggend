<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller {

	private $_per_page = 8;

	function __construct() {
		parent::__construct();

		$this->load->model('item_mdl');
	}

	function index($slug='') {
		$sort = '';
		$order = '';
		if(isset($_GET['sort'])) {
			if(strtolower($_GET['sort']) == 'cheap') {
				$sort = 'asc';
				$order = 'price';
			}
			elseif(strtolower($_GET['sort']) == 'expensive') {
				$sort = 'desc';
				$order = 'price';
			}
			elseif(strtolower($_GET['sort']) == 'old') {
				$sort = 'desc';
				$order = 'created';
			}
			elseif(strtolower($_GET['sort']) == 'new') {
				$sort = 'asc';
				$order = 'created';
			}
		}
		$config['current_page'] = (isset($_GET['page']) && $_GET['page'] > 0  ? $_GET['page'] : 1);
		$limit = $this->_per_page;
		$total_rows = 0;
		$offset = ($config['current_page'] - 1) * $this->_per_page;
		$this->data['category'] = $this->category_mdl->get_category_id_by_slug($slug);
		$param['values'] = $this->category_mdl->get_all_childs($this->data['category']->category_id);
		$param['values'][] = $this->data['category']->category_id;
		$param['field'] = 'category_id';
		$this->data['items'] = $this->item_mdl->get_items_by_category_id('', $order, $sort, $limit, $offset, $total_rows, $param);
		$config['base_url'] = base_url().$this->uri->uri_string;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $this->_per_page;
		$this->data['config'] = $config;
		// echo $config['base_url'];die();
		$this->data['content'] = 'category';
		$this->load->view('main', $this->data);

	}

	public function convert() {
		//$this->category_mdl->convert_name_to_slug();
	}
}