<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends MY_Controller {

	private $_per_page = 8;

	function __construct() {
		parent::__construct();

		$this->load->model('user_mdl');
		$this->load->model('item_mdl');
		$this->load->model('transaction_mdl');
		$this->load->config('values');
		$this->load->library('form_validation');
	}

	function index() {
		$this->data['content'] = 'home';
		$this->data['newest_items'] = $this->item_mdl->get_newest_items();
		if($this->input->post()) {
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if ($this->form_validation->run() == TRUE) {
				extract($this->input->post());
				$user['password'] = md5($password.$username.$this->config->item('user_password_salt'));
				$result = $this->user_mdl->login($user);
				if($result) {
					$this->session->set_userdata(array('username' => $username, 'user_id' => $result->user_id));
					redirect('');
				}
				else{
					$this->session->set_flashdata('error_message', 'Username dan atau Password salah');
					redirect('');
				}
			}
			else {
				$this->session->set_flashdata('error_message', 'Silahkan lengkapi data');
				redirect('');
			}
		}
		$this->load->view('main', $this->data);
	}

	function profile($username) {
		$user = $this->user_mdl->get_user_by_username(mysql_real_escape_string($username));
		if($user) {
			$user->items = $this->item_mdl->get_items($user->user_id);
		}
		else{
			$this->session->set_flashdata(array('error_message' => 'User tidak ditemukan'));
			redirect('');
		}
		$this->data['rating'] = $this->transaction_mdl->get_user_rating($user->user_id);
		$this->data['user'] = $user;
		$this->data['content'] = 'profile';
		$this->load->view('main', $this->data);

	}

	public function search() {
		$search_session = $this->session->userdata('search');

		$config['current_page'] = (isset($_GET['page']) && $_GET['page'] > 0  ? $_GET['page'] : 1);
		$limit = $this->_per_page;
		$total_rows = 0;
		$offset = ($config['current_page'] - 1) * $this->_per_page;
		$sort = '';
		$order = '';

		if($this->input->post()) {
		// var_dump($this->input->post());die();
			extract($this->input->post());
			$category_id = ($category_search != 'hide') ? $category_search : false;
			$location_id = ($location_search != 40) ? $location_search : false;
			$param = array(
					'title' => $search
				);
			$where = array();
			if($location_id){
				$where['location_id'] = $location_id;
			}
			if($category_id){
				$where['category_id'] = $category_id;
			}
			// var_dump($param);die();
			$this->data['items'] = $this->item_mdl->search($param, $where, $order, $sort, $limit, $offset, $total_rows);
			$this->data['content'] = 'search';
			$this->session->set_userdata('search', $param);
			$this->session->set_userdata('cat_loc', $where);
		}
		elseif(!empty($search_session)) {

			if(isset($_GET['sort'])) {
				if(strtolower($_GET['sort']) == 'cheap') {
					$sort = 'asc';
					$order = 'price';
				}
				elseif(strtolower($_GET['sort']) == 'expensive') {
					$sort = 'desc';
					$order = 'price';
				}
				elseif(strtolower($_GET['sort']) == 'old') {
					$sort = 'desc';
					$order = 'created';
				}
				elseif(strtolower($_GET['sort']) == 'new') {
					$sort = 'asc';
					$order = 'created';
				}
			}
			
			$this->data['items'] = $this->item_mdl->search($search_session, $this->session->userdata('cat_loc'), $order, $sort, $limit, $offset, $total_rows);
			// var_dump($this->data['items']);die();
			$this->data['content'] = 'search';
		}
		else {
			redirect('');
		}

		$config['base_url'] = base_url().$this->uri->uri_string;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $this->_per_page;
		$this->data['config'] = $config;
		$this->load->view('main', $this->data);
	}

	public function cron() {
		$this->transaction_mdl->cron_terminate_booking();
	}

}