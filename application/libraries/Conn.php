<?php

class Conn {

    public$errno;
    public $errmsg;

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->database();
        $this->errno = null;
        $this->errmsg = null;
    }

    /**
     * function ini berfungsi untuk mulai melakukan transaksi query
     */
    public function begin() {
        $this->ci->db->trans_begin();
    }

    /**
     * function ini berfungsi untuk mengakhiri transaksi query
     */
    public function end() {
        $this->ci->db->trans_end();
    }

    /**
     * function ini berfungsi untuk melakukan committed query jika transaksi query berhasil
     */
    public function commit() {
        $this->ci->db->trans_commit();
    }

    /**
     * function ini berfungsi untuk melakukan rollback jika transaksi gagal
     */
    public function rollback() {
        $this->ci->db->trans_rollback();
    }

    /**
     * function ini berfungsi jika semua query sudah dilakukan dan untuk menutup session query
     */
    public function complete() {
        $this->ci->db->trans_complete();
    }

    /**
     * function ini berfungsi untuk mengetahui status transaksi db
     */
    public function trans_status() {
        $this->ci->db->trans_status();
    }

    /**
     * function ini berfungsi untuk mengetahui status transaksi db
     */
    public function strict_mode($bool) {
        $this->ci->db->trans_strict($bool);
    }

    /**
     * function ini berfungsi untuk disable transaction
     */
    public function trans_off() {
        $this->ci->db->trans_off();
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses insert ke table-table yang ada di database
     * 
     * @param Model $object model yang strukturnya sama dengan entitiset di database (ada di folder /system/application/models)
     * @param String $table_name string dari constant yang ada di Class Tables
     * @return boolean TRUE jika proses insert berhasil <br /> FALSE jika proses insert gagal
     */
    public function save($object, $table_name, $return_id = false) {
        $result = $this->ci->db->insert($table_name, $object);
        if (!$result) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
            if ($return_id == true) {
                $result = $this->ci->db->insert_id();
            } else {
                $result = TRUE;
            }
        }
        return $result;
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses update entiti yang terdapat di entitiset tertentu di database
     * 
     * @param Model/Array $object object yang berisi nilai baru dari entiti yang akan di-update. object harus bertipe model yang strukturnya sama dengan entitiset di database (ada di folder /system/application/models) <br /><br />
     * parameter $object juga bisa diisi dengan array yang berisi korespondensi satu-satu nama field dengan nilai field dari entiti<br /><br />
     * sebaiknya gunakan Array untuk update, agar nilai yang dirubah memang benar2 field yang akan dirubah. jika menggunakan Model maka seluruh field akan dirubah nilainya sesuai dengan Model yang dipassingkan
     * @param String $table_name string dari constant yang ada di Class Tables untuk menentukan table/entitiset yang entitinya akan di-update
     * @param String/Array $where where clause yang dapat berbentuk String atau Array. kriteria tertentu dari entiti yang akan di update
     * @return boolean TRUE jika proses update berhasil <br /> FALSE jika proses update gagal
     */
    public function edit($object, $table_name, $where="") {
        if ($where != "") {
            $this->ci->db->where($where);
        }
        $result = $this->ci->db->update($table_name, $object);
        if (!$result) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
            $result = TRUE;
        }
        return $result;
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses delete entiti yang terdapat di entitiset tertentu (table) di database dengan kondisi tertentu (where)
     * 
     * @param String $table_name string dari constant yang ada di Class Tables untuk menentukan table/entitiset yang entitinya akan di-delete
     * @param String/Array $where where clause yang dapat berbentuk String atau Array. kriteria tertentu dari entiti yang akan di delete
     * @return boolean TRUE jika proses delete berhasil <br /> FALSE jika proses delete gagal
     */
    public function remove($table_name, $where) {
        $this->ci->db->where($where);
        $result = $this->ci->db->delete($table_name);
        if (!$result) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
            $result = TRUE;
        }
        return $result;
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses select count entiti-entiti yang terdapat di entitiset tertentu di database
     * 
     * @param String $table_name string dari constant yang ada di Class Tables untuk menentukan table/entitiset yang entitinya akan di-select
     * @param String/Array $where where clause yang dapat berbentuk String atau Array. kriteria tertentu dari entiti-entiti yang akan di select
     * @param String/Array $field_names field-field dari table $table_name yang akan di select
     * @param integer $limit jumlah entiti maksimal yang akan di select
     * @param integer $offset start point entiti yang akan di select
     * @param String $orderby nama field yang akan dijadikan acuan proses sorting
     * @param String $sorting jenis sorting yang digunakan (ASC/DESC)
     * @return integer/boolean jumlah row dalam integer jika proses select count berhasil <br /> FALSE jika proses select count gagal
     */
    public function countRetrievedRows($table_name, $where="", $field_names="", $limit="", $offset="", $orderby="", $sorting="", $where_in="") {
        if ($field_names != "") {
            $this->ci->db->select($field_names);
        }
        if ($where != "") {
            $this->ci->db->where($where);
        }
        if ($where_in != "") {
            $this->ci->db->where_in($where_in['field'], $where_in['values']);
        }
        if ($limit != "" && $offset != "") {
            $this->ci->db->limit($limit, $offset);
        }
        if ($orderby != "" && $sorting != "") {
            $this->ci->db->order_by($orderby, $sorting);
        }

        $result = $this->ci->db->get($table_name)->num_rows();
        if (!$result) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
        }
        return $result;
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses select entiti-entiti yang terdapat di entitiset tertentu di database
     * 
     * @param String $table_name string dari constant yang ada di Class Tables untuk menentukan table/entitiset yang entitinya akan di-select
     * @param String/Array $where where clause yang dapat berbentuk String atau Array. kriteria tertentu dari entiti-entiti yang akan di select
     * @param String/Array $field_names field-field dari table $table_name yang akan di select
     * @param boolean $as_array status nilai return apakan sebagai array atau model(model hanya berlaku ketika return hanya ada satu record)
     * @param String $orderby nama field yang akan dijadikan acuan proses sorting
     * @param String $sorting jenis sorting yang digunakan (ASC/DESC)
     * @param integer $limit jumlah entiti maksimal yang akan di select
     * @param integer $offset start point entiti yang akan di select
     * @param boolean $is_distinct menyatakan select distinct / select. (TRUE = select distinct, FALSE = select)
     * @return Resultset/boolean (model object / resultset) entities jika proses select berhasil <br /> FALSE jika proses select gagal
     */
    public function retrieve($table_name, $where="", $field_names="", $as_array=FALSE, $orderby="", $sorting="", $limit="", $offset="", $is_distinct=FALSE, $where_in="") {
        if ($field_names != "") {
            $this->ci->db->select($field_names);
        }
        if ($is_distinct) {
            $this->ci->db->distinct();
        }
        if ($where != "") {
            $this->ci->db->where($where);
        }
        if ($where_in != "") {
            $this->ci->db->where_in($where_in['field'], $where_in['values']);
        }
        if ($limit !== "" && $offset !== "") {
            $this->ci->db->limit($limit, $offset);
        }
        if ($orderby != "" && $sorting != "") {
            $this->ci->db->order_by($orderby, $sorting);
        }
		$result = array();
		
        //$result = $this->ci->db->get($table_name)->result();
		$query = $this->ci->db->get($table_name);
		$last_query = $this->ci->db->last_query();
        // echo $last_query;die();
		if(is_object($query)){
			$result = $query->result();
		}else{
		
			$res = (is_null($query))?'is null':$query;
			//echo $table_name;
			//echo $res;
			//echo $last_query;exit;
		}
		
        if (!is_array($result)) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
            if ($as_array == FALSE) {
                if (sizeof($result) === 1) {
                    $result = $result[0];
                } else if (sizeof($result) === 0) {
                    $result = null;
                }
            } else {
                if (sizeof($result) === 0) {
                    $result = array();
                }
            }
        }
        return $result;
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses DML/CRUD dengan query yang ditentukan oleh parameter $query
     * 
     * @param String $query string query yang akan di-run
     * @return Resultset/boolean resultset jika $query berhasil diproses <br /> FALSE jika $query gagal diproses
     */
    public function nativeQuery($query, $update=false) {
        if ($update == true) {
            $result = $this->ci->db->query($query);
        } else {
            $result = $this->ci->db->query($query)->result();
        }
        if (!$result) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
        }
        return $result;
    }

    public function count_affected_rows(){
        return $this->ci->db->affected_rows();
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses select pada entiti set tertentu dengan kriteria where clause menggunakan LIKE .. OR LIKE
     * 
     * @param String $table_name string dari constant yang ada di Class Tables untuk menentukan table/entitiset yang entitinya akan di-select
     * @param String/Array $field_names field-field dari table $table_name yang akan di select
     * @param Array $where where clause yang dapat berbentuk String atau Array. kriteria tertentu dari entiti-entiti yang akan di select
     * @param integer $limit jumlah entiti maksimal yang akan di select
     * @param integer $offset start point entiti yang akan di select
     * @param String $orderby nama field yang akan dijadikan acuan proses sorting
     * @param String $sorting jenis sorting yang digunakan (ASC/DESC)
     * @return Resultset/boolean resultset jika proses searching berhasil <br /> FALSE jika proses searching gagal
     */
    public function search($table_name, $where, $where_equal, $field_names="", $orderby="", $sorting="", $limit="", $offset="", $is_distinct=FALSE) {
        if ($field_names != "") {
            $this->ci->db->select($field_names);
        }
        if ($is_distinct) {
            $this->ci->db->distinct();
        }
        /*         * coba* */
        $i = 1;
        foreach ($where as $key => $val) {
            if ($i == 1) {
                $this->ci->db->like($key, $val);
            } else {
                $this->ci->db->or_like($key, $val);
            }
            $i++;
        }
        // var_dump($where_equal);die();
        if ($where_equal != "") {
            $this->ci->db->where($where_equal);
        }
        if ($limit !== "" && $offset !== "") {
            $this->ci->db->limit($limit, $offset);
        }
        if ($orderby != "" && $sorting != "") {
            $this->ci->db->order_by($orderby, $sorting);
        }
        $result = $this->ci->db->get($table_name)->result();
        if (!is_array($result)) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
        }
        return $result;
    }

    /**
     * function ini berfungsi sebagai interface yang digunakan aplikasi untuk proses select count pada entiti set tertentu dengan kriteria where clause menggunakan LIKE .. OR LIKE
     * 
     * @param String $table_name string dari constant yang ada di Class Tables untuk menentukan table/entitiset yang entitinya akan di-select
     * @param Array $where where clause yang dapat berbentuk String atau Array. kriteria tertentu dari entiti-entiti yang akan di select
     * @return integer/boolean jumlah row dalam integer jika proses select count berhasil <br /> FALSE jika proses select count gagal
     */
    public function countSearchedRows($table_name, $where) {
        $i = 1;
        foreach ($where as $key => $val) {
            if ($i == 1) {
                $this->ci->db->like($key, $val);
            } else {
                $this->ci->db->or_like($key, $val);
            }
            $i++;
        }

        $result = $this->ci->db->get($table_name)->num_rows();
        if (!$result) {
            $this->errno = $this->ci->db->_error_number();
            $this->errmsg = $this->ci->db->_error_message();
            $result = FALSE;
        } else {
            $this->errno = null;
            $this->errmsg = null;
        }
        return $result;
    }

    public function __destruct() {
        $this->ci->db->close();
    }

}