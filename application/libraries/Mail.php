<?php

class Mail {
	private $_from = "From:cs@jualbeliyuk.com\r\n";

	public function send_email($subject, $message, $to=false){
		$_to = (LOCAL_MODE) ? 'postmaster@localhost' : 'admin@jualbeliyuk.com';
		$to = (!$to) ? $_to : $to;

		$message = '<html><head><meta charset="utf-8"></head><body>'.$message.'</body></html>';
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		if(mail($to, $subject, $message, $this->_from.$headers)) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}
?>