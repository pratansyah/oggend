<?php
	class MY_Controller extends CI_Controller {
		private $_category;
		private $_location;
		protected $data;
		public function __construct() {
			parent::__construct();

			$this->load->model('category_mdl');
			$this->load->model('location_mdl');
			$this->load->model('user_mdl');
			$this->load->library('pagination');

			$this->get_categories();
			$this->get_locations();

			$this->data['method'] = $this->get_current_method();
			date_default_timezone_set('Asia/Jakarta');

			if($this->session->userdata('username') && !$this->session->userdata('admin')) {
				$this->get_profile($this->session->userdata('username'));
			}
			else {
				$this->data['profile'] = FALSE;
			}

		}

		public function get_categories() {
			$this->data['categories'] = $this->category_mdl->get_categories();
		}

		public function get_locations() {
			$this->data['locations'] = $this->location_mdl->get_provinces();
		}

		public function get_current_method() {
			return $this->uri->segment(1).'_'.$this->uri->segment(2);
		}

		public function get_profile($username) {
			$this->data['profile'] = $this->user_mdl->get_user_by_username($username);
		}
	}
?>