<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['user_password_salt'] = 'cUn6uK-T3rB4ng%!!';
$config['avatar_real_path'] = realpath(APPPATH . '../images/avatar/');
$config['confirmation_real_path'] = realpath(APPPATH . '../images/confirmation/');
$config['product_image_real_path'] = realpath(APPPATH . '../images/product/');
?>