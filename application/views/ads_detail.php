<div id="content">
    <div class="container">
        <div class="single-product">
            <div class="category-head">
                <nav class="breadcrumbs" itemprop="breadcrumb">
                    <span class="trail-begin"><a href="<?php echo base_url().'category'; ?>" title="" rel="home" class="trail-begin">kategori utama</a></span> 
                    <?php if(isset($category->parent_cat->parent_cat->name)): ?>
                        <span class="sep">/</span> 
                        <a href="<?php echo base_url().'category/'.$category->parent_cat->parent_cat->slug ?>" title="2013" class="trail-end"><?php echo $category->parent_cat->parent_cat->name; ?></a> 
                    <?php endif; ?>
                    <?php if(isset($category->parent_cat->name)): ?>
                        <span class="sep">/</span> 
                        <a href="<?php echo base_url().'category/'.$category->parent_cat->slug ?>" title="2013" class="trail-end"><?php echo $category->parent_cat->name; ?></a> 
                    <?php endif; ?>
                    <span class="trail-current"><?php echo $category->name; ?></span>
                </nav><!-- End .breadcrumbs -->

                <nav class="sub-category">
                    <ul>
                        <?php foreach($category->child as $child): ?>
                        <li><a href="<?php echo base_url().'category/'.$child->slug; ?>"><?php echo $child->name; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </nav><!-- End .sub-category -->
            </div><!-- End .category-head -->
            <div class="row single-detail">
                <div class="col-2 pull-left images">

                    <div class="images">
                        <img id="zoom_03" src="<?php echo items_url().'small/'; echo (isset($detail['item']->image[0]->image)) ? $detail['item']->image[0]->image : 'default.png';?>" data-zoom-image="<?php echo items_url().'big/'; echo (isset($detail['item']->image[0]->image)) ? $detail['item']->image[0]->image : 'default.png'; ?>">

                        <div id="gallery_01">
                            <?php if(isset($detail['item']->image[0]->image)): foreach($detail['item']->image as $image): ?>
                            <a href="#" class="elevatezoom-gallery" data-update="" data-image="<?php echo items_url().'small/'.$image->image; ?>" data-zoom-image="<?php echo items_url().'big/'.$image->image; ?>">
                                <img src="<?php echo items_url().'small/'.$image->image; ?>" width="117">
                            </a>
                            <?php endforeach; endif; ?>

                        </div>
                    </div>
                    <div class="description">
                        <?php echo $detail['item']->description; ?>
                    </div>

                </div>
                <div class="col-2 pull-left summary">
                    <!-- <nav class="breadcrumbs" itemprop="breadcrumb">
                        <span class="trail-begin"><a href="#" title="" rel="home" class="trail-begin">mobil</a></span> 
                        <span class="sep">></span> 
                        <a href="#" title="2013" class="trail-end">mobil bekas</a> 
                        <span class="sep">></span> 
                        <span class="trail-current">BMW</span>
                    </nav> -->
                    <span class="product-title"><?php echo $detail['item']->title; ?></span>
                    <span class="price">Rp. <?php echo number_format($detail['item']->price, 0, ',', '.'); ?> (Exclude Shipping)</span>
                    <div class="seller-profile">
                        <a href="<?php echo base_url()."user/".$detail['user']->username; ?>">
                            <figure class="avatar">
                                <img src="<?php echo avatar_url().$detail['user']->avatar; ?>" alt="">
                            </figure>
                            <div class="about-seller">
                                <span class="name"><?php echo $detail['user']->fullname; ?></span>
                                <span class="location"><?php echo $detail['user']->location_name; ?></span>
                                <div class="since"><span>member sejak:</span> <?php echo date('d M Y', strtotime($detail['user']->created)); ?></div>
                            </div>
                        </a>
                    </div>
                    <div class="ad-detail">
                        <span class="location"><i class="fa fa-map-marker"></i><?php echo $detail['item']->location_name; ?></span>
                        <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($detail['item']->created), time()); ?></span>
                    </div>
                    <?php if($detail['item']->shipped == 1 && $detail['item']->booked == 'f'): ?>
                        <div href="#" id="book" class="button yellow" style="background: #ded021;" data-base_url="<?php echo base_url(); ?>" data-token="<?php echo $detail['item']->booking_token; ?>" data-item_id="<?php echo $detail['item']->item_id; ?>">Beli Sekarang</div>
                    <?php elseif($detail['item']->shipped == 1 && $detail['item']->booked == 't'): ?>
                        <div href="#" class="button yellow" style="background: grey; color: white;">Telah dipesan</div>
                    <?php else: ?>
                        <div href="#" class="button yellow">Hubungi Seller</div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if($same_category): ?>
            <section class="related">
                <h3>barang sejenis</h3>
                <div class="catalog-products">
                    <ul class="view-thumbnail">
                        <?php foreach($same_category as $item): ?>
                        <li>
                            <a href="<?php echo base_url().'/ads/detail/'.$item['item']->permalink; ?>">
                                <figure>
                                    <img src="<?php echo items_url().'thumb/'; echo (isset($item['item']->image[0]->image)) ? $item['item']->image[0]->image : 'default.png';?>" alt="">
                                </figure>
                                <div class="summary">
                                    <span class="product-title"><?php echo (strlen($item['item']->title) < 20 ) ? $item['item']->title : substr($item['item']->title, 0, 20).'...'; ?></span>
                                    <div class="detail-summary">
                                        <span class="price">Rp. <?php echo number_format($item['item']->price, 0, ',', '.'); ?></span>
                                        <span class="status"><?php echo ($item['item']->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                                    </div>
                                </div>
                                <div class="footer-summary">
                                    <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($item['item']->created), time()); ?></span></br>
                                    <span class="location" style="float: left;"><i class="fa fa-map-marker"></i><?php echo ($item['item']->location_name != 'nowhere') ? $item['item']->location_name : ''; ?></span>
                                </div>     
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div><!-- End .catalog-products -->
            </section>
            <?php endif; ?>
        </div>
    </div>

</div>