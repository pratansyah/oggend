<div id="content">
    <div class="container">
        <div class="products">
            <div class="category-head">
                <nav class="breadcrumbs" itemprop="breadcrumb">
                    <span class="trail-begin"><a href="<?php echo base_url().'category'; ?>" title="" rel="home" class="trail-begin">kategori utama</a></span> 
                    <span class="trail-current">Pencarian</span>
                </nav><!-- End .breadcrumbs -->
            </div><!-- End .category-head -->

            <div class="products-head">
                <div class="view">
                    <span class="view-option thumbnail-view "><i class="fa fa-th-large"></i></span>
                    <span class="view-option list-view"><i class="fa fa-th-list"></i></span>
                </div>
                <select id="urutan" data-url="<?php echo base_url().'search'; ?>" data-function="sort">
                    <option value="normal">Urutkan</option>
                    <option value="cheap">Harga termurah</option>
                    <option value="expensive">Harga termahal</option>
                    <option value="new">Iklan terbaru</option>
                    <option value="old">iklan terlama</option>
                </select> 
            </div><!-- End .products-head -->

            <div class="catalog-products">
                <ul>
                <?php
                    if($items == NULL) :
                        echo "Tidak ada produk dalam kategori ini";
                    else :
                        foreach($items as $item): 
                ?>
                    <li>
                        <a href="<?php echo base_url().'ads/detail/'.$item['item']->permalink; ?>">
                            <figure>
                                <img src="<?php echo items_url().'thumb/'; echo (isset($item['item']->image[0]->image)) ? $item['item']->image[0]->image : 'default.png';?>" alt="">
                            </figure>
                            <div class="summary">
                                <span class="product-title"><?php echo $item['item']->title; ?></span>
                                <div class="detail-summary">
                                    <span class="price">Rp. <?php echo number_format($item['item']->price, 0, ',', '.'); ?></span>
                                    <span class="status"><?php echo ($item['item']->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                                </div>
                            </div>
                            <div class="footer-summary">
                                <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($item['item']->created), time()); ?></span>
                                <span class="location"><i class="fa fa-map-marker"></i><?php echo $item['item']->location_name; ?></span>
                            </div>     
                        </a>
                    </li>
                <?php endforeach; endif; ?>
                </ul>
            </div><!-- End .catalog-products -->


            <?php front_pagination($config); ?>
        </div>
    </div>

</div>