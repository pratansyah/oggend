<div id="content">
    <div class="container">
        <div class="products">
            <div class="category-head">
                <nav class="breadcrumbs" itemprop="breadcrumb">
                    <span class="trail-begin"><a href="<?php echo base_url().'category'; ?>" title="" rel="home" class="trail-begin">kategori utama</a></span> 
                    <?php if(isset($category->parent_cat->parent_cat->name)): ?>
                        <span class="sep">/</span> 
                        <a href="<?php echo base_url().'category/'.$category->parent_cat->parent_cat->slug ?>" title="2013" class="trail-end"><?php echo $category->parent_cat->parent_cat->name; ?></a> 
                    <?php endif; ?>
                    <?php if(isset($category->parent_cat->name)): ?>
                        <span class="sep">/</span> 
                        <a href="<?php echo base_url().'category/'.$category->parent_cat->slug ?>" title="2013" class="trail-end"><?php echo $category->parent_cat->name; ?></a> 
                    <?php endif; ?>
                    <span class="trail-current"><?php echo $category->name; ?></span>
                </nav><!-- End .breadcrumbs -->

                <nav class="sub-category">
                    <ul>
                        <?php foreach($category->child as $child): ?>
                        <li><a href="<?php echo base_url().'category/'.$child->slug; ?>"><?php echo $child->name; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </nav><!-- End .sub-category -->
            </div><!-- End .category-head -->

            <div class="products-head">
                <div class="view">
                    <span class="view-option thumbnail-view "><i class="fa fa-th-large"></i></span>
                    <span class="view-option list-view"><i class="fa fa-th-list"></i></span>
                </div>
                <select id="urutan" data-url="<?php echo base_url().'category/'.$category->slug; ?>" data-function="sort">
                    <option value="normal">Urutkan</option>
                    <option value="cheap">Harga termurah</option>
                    <option value="expensive">Harga termahal</option>
                    <option value="new">Iklan terbaru</option>
                    <option value="old">iklan terlama</option>
                </select> 
            </div><!-- End .products-head -->

            <div class="catalog-products">
                <ul>
                <?php
                    if($items == NULL) :
                        echo "Tidak ada produk dalam kategori ini";
                    else :
                        foreach($items as $item): 
                ?>
                    <li>
                        <a href="<?php echo base_url().'ads/detail/'.$item['item']->permalink; ?>">
                            <figure>
                                <img src="<?php echo items_url().'thumb/'; echo (isset($item['item']->image[0]->image)) ? $item['item']->image[0]->image : 'default.png'; ?>" alt="">
                            </figure>
                            <div class="summary">
                                <span class="product-title"><?php echo (strlen($item['item']->title) < 20 ) ? $item['item']->title : substr($item['item']->title, 0, 20).'...'; ?></span>
                                <div class="detail-summary">
                                    <span class="price">Rp. <?php echo number_format($item['item']->price, 0, ',', '.'); ?></span>
                                    <span class="status"><?php echo ($item['item']->condition == 1) ? 'Baru' : 'Bekas'; ?></span>
                                </div>
                            </div>
                            <div class="footer-summary">
                                <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($item['item']->created), time()); ?></span></br>
                                <span class="location" style="float: left;"><i class="fa fa-map-marker"></i><?php echo $item['item']->location_name; ?></span>
                            </div>     
                        </a>
                    </li>
                <?php endforeach; endif; ?>
                </ul>
            </div><!-- End .catalog-products -->

            <?php front_pagination($config); ?>
            <!-- <nav class="paging cl" role="navigation">
                <a href="#" class="prev">Prev</a>
                <a href="#">1</a>
                <span class="current">2</span>
                <a href="#">3</a>
                <span class="hellip">&hellip;</span>
                <a href="#">99</a>
                <a href="#" class="last">100</a>
                <a href="#" class="next">Next</a>
            </nav> -->
        </div>
    </div>

</div>