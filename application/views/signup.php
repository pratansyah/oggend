<div id="content">
    <div class="container">
        <div class="login-signup-wrapper">
            <div class="login">
                <form id="login" method="POST" action="<?php echo base_url(); ?>">
                    <h3>Login</h3>
                    <p class="white-bg">
                        <input type="text" class="input username" placeholder="username" name="username">
                        <input type="password" class="input password" placeholder="password" name="password">
                    </p>
                    <p class="signin">
                        <input type="submit" value="sign in">
                    </p>
                </form>
            </div>
            <div class="sign-up">

                <form action="<?php echo base_url().'signup'; ?>" class="register-form" method="POST">
                    <div class="head-title">
                        <h3 class="section-title">Daftar</h3>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Nama Lengkap</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="text" id="full-name" name="full_name" class="input-text">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Username</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="text" id="username" name="username" class="input-text">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">No Hp</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="text" id="no_hp" name="no_hp" class="input-text">
                        </div>
                    </div>
                    <div class="row-fluid radio">
                        <label class="control-label">jenis-kelamin</label>
                        <input type="radio" id="r1" name="rgroup" checked value="M">
                        <label for="r1"><span></span>laki-laki</label>
                        
                        <input type="radio" id="r2" name="rgroup" value="F">
                        <label for="r2"><span></span>perempuan</label>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Alamat Email</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="email" id="email" name="email" class="input-text">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Kata sandi</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="password" id="password" name="password" class="input-text">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">ulangi kata sandi</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="password" id="password_repeat" name="password_repeat" class="input-text">
                        </div>
                    </div>
                    <div class="row-fluid chck">
                        <input type="checkbox" id="c1">
                        <label for="c1"><span></span></label>
                        <small>Saya sudah membaca dan menyetujui <a href="#" target="_blank">Syarat dan Ketentuan</a>, serta <a href="#" target="_blank">Kebijakan Privasi</a> JualBeliYuk.com</small>
                    </div>
                    <input type="submit" class="button" value="Daftar" disabled="true" id="submit_signup">
                </form>
            </div>
        </div>
    </div>
</div>