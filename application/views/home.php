<div id="content">
    <div class="container">
        <div class="thumbnail-cat">
            <div class="head-title">
                <h3 class="section-title">kategori pilihan</h3>
            </div>
            <div class="main-category"> 
                <ul>
                    <li><a href="<?php echo base_url().'category/mobil'; ?>"><img src="<?php assets_url(); ?>img/mobil.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/motor'; ?>"><img src="<?php assets_url(); ?>img/motor.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/keperluan-pribadi'; ?>"><img src="<?php assets_url(); ?>img/pribadi.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/properti'; ?>"><img src="<?php assets_url(); ?>img/properti.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/rumah-tangga'; ?>"><img src="<?php assets_url(); ?>img/rumahtangga.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/elektronik'; ?>"><img src="<?php assets_url(); ?>img/elektronik.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/hobi'; ?>"><img src="<?php assets_url(); ?>img/hobi.png" alt=""></a></li>
                    <li><a href="<?php echo base_url().'category/bayi-anak'; ?>"><img src="<?php assets_url(); ?>img/bayi.png" alt=""></a></li>
                </ul>
            </div>
        </div>
        <div class="sidebar">
            <div class="login">
            <a href="<?php echo base_url().'dashboard/create_ads'; ?>" class="button orange">Pasang Iklan gratis disini</a>
            <?php if(!$profile): ?>
                    <form id="login" method="POST">
                        <h3>Login</h3>
                        <p class="white-bg">
                            <input type="text" class="input username" placeholder="username" name="username">
                            <input type="password" class="input password" placeholder="password" name="password">
                        </p>
                        <p class="signin">
                            <input type="submit" value="sign in">
                        </p>
                        <p class="register">
                            <a href="<?php echo base_url().'signup'; ?>" class="create-account">Belum punya akun?</a>
                            <a href="<?php echo base_url().'signup'; ?>" class="button orange">register</a>
                        </p>
                    </form>
            <?php else: ?>
                <figure class="avatar">
                    <img src="<?php echo avatar_url().$profile->avatar; ?>" alt="">
                </figure>
                <div class="seller-profile">
                    <div class="about-seller">
                        <span class="name"><a href="<?php echo base_url().'user/'.$profile->username; ?>"><?php echo $profile->fullname; ?></a></span>
                        <span class="location"><?php echo $profile->location_name; ?></span>
                        <div class="since"><span>member sejak:</span> <?php echo date('d M Y', strtotime($profile->created)); ?></div>
                    </div>
                </div>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <?php if($newest_items): ?>
        <div class="latest-sales">
            <div class="head-title">
                <h3 class="section-title">barang terbaru</h3>
            </div>
            <div class="slider-product">
                <?php foreach($newest_items as $item): ?>
                <div class="thumbnail-product"><img src="<?php echo items_url().'thumb/'; echo (isset($item['item']->image[0]->image)) ? $item['item']->image[0]->image : 'default.png'; ?>" alt=""></div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>

        <section id="super-sales">
            <ul class="pin-grid">
                <?php if($newest_items): foreach($newest_items as $item): ?>
                <li>
                    <a href="<?php echo base_url().'ads/detail/'.$item['item']->permalink; ?>">
                        <figure>
                            <img src="<?php echo items_url().'thumb/'; echo (isset($item['item']->image[0]->image)) ? $item['item']->image[0]->image : 'default.png';?>" alt="">
                        </figure>
                        <div class="summary">
                            <span class="product-title"><?php echo (strlen($item['item']->title) < 20 ) ? $item['item']->title : substr($item['item']->title, 0, 20).'...'; ?></span>
                            <div class="detail-summary">
                                <span class="price">Rp. <?php echo number_format($item['item']->price, 0, ',', '.'); ?></span>
                                <span class="status"><?php echo ($item['item']->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                            </div>
                        </div>
                        <div class="footer-summary">
                            <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($item['item']->created), time()); ?></span></br>
                            <span class="location" style="float: left;"><i class="fa fa-map-marker"></i><?php echo $item['item']->location_name; ?></span>
                        </div>     
                    </a>
                </li>
                <?php endforeach; endif; ?>
            </ul>
        </section>
    </div>

</div>