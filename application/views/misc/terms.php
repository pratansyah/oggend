<div class="container">
	<div class="page-template">
	    <div class="head-title">
	        <h3 class="section-title">term and condition</h3>
	    </div>
	    <div class="entry-content">
	        <h3>Selamat datang di www.jualbeliyuk.com</h3>

	        <p>Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh JualBeliYuk.com yang berada dibawah badan hukum CV. INDONIAGA JAYA BANGUN terkait penggunaan situs www.jualbeliyuk.com. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.</p>

	        <p>Dengan mendaftar dan/atau menggunakan situs www.jualbeliyuk.com, maka pengguna dianggap telah membaca, mengerti, memahami dan menyutujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan www.jualbeliyuk.com. Jika pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di www.jualbeliyuk.com.</p>
	        
	        <h4>A. Definisi</h4>
	        <h4>B. Akun, Saldo Tokopedia, Password dan Keamanan</h4>
	        <h4>C. Transaksi Pembelian</h4>
	        <h4>D. Transaksi Penjualan</h4>
	        <h4>E. Harga</h4>
	        <h4>F. Tarif Pengiriman</h4>
	        <h4>G. Konten</h4>
	        <h4>H. Jenis Barang</h4>
	        <h4>I. Iklan</h4>
	        <h4>J. Pengiriman Barang</h4>
	        <h4>K. Penarikan Dana</h4>
	        <h4>L. Pembaharuan</h4>


	        <h3>A. Definisi</h3>
	        <ol>
	        	<li>www.jualbeliyuk.com adalah suatu layanan jasa yang menjalankan kegiatan usaha jasa web portal yang berada dibawah badan hukum, CV. INDONIAGA JAYA BANGUN, yakni situs jual beli barang baik bekas maupun barang baru.</li>
	        	<li>Syarat & ketentuan adalah perjanjian antara Pengguna dan JualBeliYuk.com yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab pengguna dan JualBeliYuk.com, serta tata cara penggunaan sistem layanan JualBeliYuk.com.</li>
	        	<li>Pengguna adalah pihak yang menggunakan layanan Tokopedia, termasuk namun tidak terbatas pada pembeli, penjual ataupun pihak lain yang sekedar berkunjung ke Situs JualBeliYuk.com.</li>
	        	<li>Pembeli adalah Pengguna terdaftar yang melakukan permintaan atas Barang yang dijual oleh Penjual di Situs JualBeliYuk.com.</li>
	        	<li>Penjual adalah Pengguna terdaftar yang melakukan penawaran atas suatu Barang kepada para Pengguna Situs JualBeliYuk.com.</li>
	        	<li>Barang adalah benda yang berwujud / memiliki fisik Barang yang dapat diantar / memenuhi kriteria pengiriman oleh perusahaan jasa pengiriman Barang.</li>
	        	<li>Rekening Resmi JualBeliYuk.com adalah rekening yang disepakati oleh JualBeliYuk.com dan para pengguna untuk proses transaksi. jual beli di Situs JualBeliYuk.com. Rekening resmi JBY dapat ditemukan di halaman.</li>
	        </ol>
	        
	        <h3>B.	AKUN, PASSWORD, DAN KEAMANAN</h3>
	        <ol>
	        	<li>Pengguna dengan ini menyatakan bahwa pengguna adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut hukum.</li>
	        	<li>JBY tidak memungut biaya apapun baik pendaftaran maupun pasang iklan kepada Pengguna.</li>
	        	<li>Pengguna yang telah mendaftar berhak bertindak sebagai:
	        		<ul>
	        			<li>Pembeli</li>
	        			<li>Penjual, dengan memanfaatkan layanan pasang iklan</li>
	        		</ul>
	        	</li>
	        	<li>JBY tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat & ketentuan dan/atau hukum yang berlaku, yakni tindakan berupa penghapusan iklan, suspensi akun, dan/atau penghapusan akun pengguna.</li>
	        	<li>JBY memiliki kewenangan untuk menutup akun Pengguna baik sementara maupun permanen apabila didapati adanya tindakan kecurangan dalam bertransaksi untuk kepentingan pribadi Pengguna.</li>
	        	<li>JBY memiliki kewenangan untuk melakukan penyesuaian jumlah transaksi dan/atau melakukan proses moderasi/menutup akun Pengguna, jika diketahui atau diduga adanya kecurangan oleh Pengguna yang bertujuan memanipulasi data transaksi Pengguna demi meningkatkan reputasi akun (review dan atau jumlah transaksi). Contohnya adalah melakukan proses belanja ke toko sendiri dengan menggunakan akun pribadi atau akun pribadi lainnya.</li>
	        	<li>Pengguna bertanggung jawab secara pribadi untuk menjaga kerahasiaan akun dan password untuk semua aktivitas yang terjadi dalam akun Pengguna.</li>
	        	<li>JBY tidak akan meminta password akun Pengguna untuk alasan apapun, oleh karena itu JBY menghimbau Pengguna agar tidak memberikan password akun Anda kepada pihak manapun, baik kepada pihak ketiga maupun kepada pihak yang mengatasnamakan JBY.</li>
	        	<li>Pengguna setuju untuk memastikan bahwa Pengguna keluar dari akun di akhir setiap sesi dan memberitahu JBY jika ada penggunaan tanpa izin atas sandi atau akun Pengguna.</li>
	        	<li>Pengguna dengan ini menyatakan bahwa JBY tidak bertanggung jawab atas kerugian atau kerusakan yang timbul dari penyalahgunaan akun Pengguna.</li>
	        </ol>

	        <h3>C.	TRANSAKSI PEMBELIAN</h3>
	        <ol>
	        	<li>Metode pembayaran wajib menggunakan Rekening Resmi JualBeliYuk. Pembeli melakukan transfer sejumlah uang sesuai harga ke rekening resmi Tokopedia, setelah itu JualBeliYuk akan melanjut kirimkan uang tersebut ke pihak penjual setelah tahap-tahap sistem jual beli Tokopedia selesai.</li>
	        	<li>Saat melakukan pembelian Barang, Pembeli menyetujui bahwa:
	        		<ul>
	        			<li>Pembeli bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan Barang (termasuk didalamnya namun tidak terbatas pada warna, kualitas, fungsi, dan lainnya) sebelum membuat komitmen untuk membeli.</li>
	        			<li>Pembeli mengakui bahwa warna sebenarnya dari produk sebagaimana terlihat di situs JualBeliYuk tergantung pada monitor komputer Pembeli. JualBeliYuk telah melakukan upaya terbaik untuk memastikan warna dalam foto-foto yang ditampilkan di Situs JualBeliYuk.com muncul seakurat mungkin, tetapi tidak dapat menjamin bahwa penampilan warna pada Situs JualBeliYuk.com akan akurat.</li>
	        			<li>Pengguna masuk ke dalam kontrak yang mengikat secara hukum untuk membeli Barang ketika Pengguna membeli suatu barang.</li>
	        			<li>JualBeliYuk tidak mengalihkan kepemilikan secara hukum atas barang-barang dari Penjual kepada Pembeli.</li>
	        		</ul>
	        	</li>
	        	<li>Pembeli memahami dan menyetujui bahwa ketersediaan stok Barang merupakan tanggung jawab Penjual yang menawarkan Barang tersebut. Terkait ketersediaan stok Barang dapat berubah sewaktu-waktu, sehingga dalam keadaan stok Barang kosong, maka penjual akan menolak order, dan pembayaran atas barang yang bersangkutan dikembalikan kepada Pembeli apabila pembeli sudah melakukan pembayaran.</li>
	        	<li>Pembeli memahami sepenuhnya dan menyetujui bahwa segala transaksi yang dilakukan antar Pembeli dan Penjual selain melalui Rekening Resmi JualBeliYuk dan/atau tanpa sepengetahuan JualBeliYuk (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus diluar situs JualBeliYuk atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari Pembeli.</li>
	        	<li>JualBeliYuk memiliki kewenangan sepenuhnya untuk menolak pembayaran tanpa pemberitahuan terlebih dahulu.</li>
	        	<li>Pembayaran oleh Pembeli wajib dilakukan segera (selambat-lambatnya dalam batas waktu 3 jam) setelah Pembeli melakukan PESAN BARANG. Jika dalam batas waktu tersebut pembayaran atau konfirmasi pembayaran belum dilakukan oleh pembeli, JualBeliYuk memiliki kewenangan untuk membatalkan transaksi dimaksud. Pengguna tidak berhak mengajukan klaim atau tuntutan atas pembatalan transaksi tersebut.</li>
	        	<li>Konfirmasi pembayaran dengan setoran tunai wajib disertai dengan nomer token dan upload foto bukti pembayaran(ATM, Internet Banking, dsb). </li>
	        	<li>Pembeli wajib melakukan konfirmasi penerimaan Barang, setelah menerima kiriman Barang yang dibeli. JualBeliYuk memberikan batas waktu 1 (satu) hari setelah waktu pengiriman yang dapat dilihat dari resi pengiriman barang (Contoh: Di Resi diperkirakan sampai Hari Jumat, maka diberikan waktu sampai hari Sabtu untuk melakukan konfirmasi barang diterima). Jika dalam batas waktu tersebut tidak ada konfirmasi atau klaim dari pihak Pembeli, maka dengan demikian Pembeli menyatakan menyetujui dilakukannya konfirmasi penerimaan Barang secara otomatis oleh sistem JualBeliYuk.com.</li>
	        	<li>Setelah adanya konfirmasi penerimaan Barang atau konfirmasi penerimaan Barang otomatis, maka dana pihak Pembeli yang dikirimkan ke Rekening resmi JualBeliYuk akan di lanjut kirimkan ke pihak Penjual (transaksi dianggap selesai).</li>
	        	<li>Pembeli memahami dan menyetujui bahwa setiap klaim yang dilayangkan setelah adanya konfirmasi / konfirmasi otomatis penerimaan Barang adalah bukan menjadi tanggung jawab JualBeliYuk.com. Kerugian yang timbul setelah adanya konfirmasi/konfirmasi otomatis penerimaan Barang menjadi tanggung jawab Pembeli secara pribadi.</li>
	        	<li>Pembeli memahami dan menyetujui bahwa setiap masalah pengiriman Barang yang disebabkan keterlambatan pembayaran adalah merupakan tanggung jawab dari Pembeli.</li>
	        	<li>Pembeli memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank yang Pembeli pergunakan dengan bank Rekening resmi JualBeliYuk adalah tanggung jawab Pembeli secara pribadi.</li>
	        	<li>Pengembalian dana dari JualBeliYuk kepada Pembeli hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:
	        		<ul>
	        			<li>Kelebihan pembayaran dari Pembeli atas harga Barang,</li>
	        			<li>Masalah pengiriman Barang telah teridentifikasi secara jelas dari Penjual yang mengakibatkan pesanan Barang tidak sampai,</li>
	        			<li>Penjual tidak bisa menyanggupi order karena kehabisan stok, perubahan ongkos kirim, maupun penyebab lainnya,</li>
	        			<li>Penjual sudah menyanggupi pengiriman order Barang, tetapi setelah batas waktu yang ditentukan ternyata Penjual tidak mengirimkan Barang hingga batas waktu yang telah ditentukan.</li>
	        		</ul>
	        	</li>
	        	<li>Apabila terjadi proses pengembalian dana dari JualBeliYuk maka akan langsung dikirimkan ke pengguna sesuai dengan jumlah pengembalian dana.</li>
	        	<li>Transaksi hanya dapat dibatalkan sebelum Pembeli melakukan konfirmasi pembayaran dan disampaikan secara resmi kepada JualBeliYuk melalui metode yang tersedia, kecuali dalam kondisi adanya pembatalan dari pihak Penjual.</li>
	        	<li>JualBeliYuk berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Penjual dan Pembeli, dengan melihat bukti-bukti yang ada. Keputusan Tokopedia adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Penjual dan Pembeli untuk mematuhinya.</li>
	        </ol>

	        <h3>D.	TRANSAKSI PENJUALAN</h3>
	        <ol>
	        	<li>Penjual dilarang memanipulasi harga Barang dengan tujuan apapun.</li>
	        	<li>Penjual dilarang melakukan penawaran / berdagang Barang terlarang sesuai dengan yang telah ditetapkan pada ketentuan "Jenis Barang".</li>
	        	<li>Penjual wajib memberikan balasan untuk menerima atau menolak pesanan Barang pihak Pembeli dalam batas waktu 1 hari setelah adanya notifikasi pemesanan Barang dari JualBeliYuk. Jika dalam batas waktu tersebut tidak ada balasan dari Penjual maka secara otomatis pesanan dianggap dibatalkan.</li>
	        	<li>Setelah menyetujui untuk menerima pesanan pembeli, maka pihak Penjual wajib memasukan nomor resi pengiriman Barang dalam batas waktu 2 hari. Jika dalam batas waktu tersebut pihak Penjual tidak memasukan nomor resi pengiriman Barang maka secara otomatis pesanan dianggap dibatalkan.</li>
	        	<li>Penjual memahami dan menyetujui bahwa pembayaran atas harga Barang dan ongkos kirim (diluar biaya transfer / administrasi) akan dikembalikan sepenuhnya ke Pembeli apabila transaksi dibatalkan dan/atau resi pengiriman Barang tidak valid dan/atau ketentuan lain yang diatur dalam Syarat & ketentuan No C 13</li>
	        	<li>Dalam keadaan Penjual hanya dapat memenuhi sebagian dari jumlah Barang yang dipesan oleh Pembeli, maka Penjual wajib memberikan keterangan kepada JualBeliYuk sebelum menerima pesanan dimaksud. Pembeli memiliki kewenangan penuh untuk menyetujui melanjutkan transaksi/membatalkan transaksi dan Penjual dilarang melanjutkan transaksi tanpa mendapat persetujuan dari Pembeli. Apabila telah disetujui ulang oleh Pembeli sesuai dengan jumlah pesanan yang disanggupi oleh Penjual, maka selisih dana total harga Barang akan dikembalikan kepada pihak Pembeli.</li>
	        	<li>JualBeliYuk memiliki kewenangan untuk menahan pembayaran dana di Rekening Resmi JualBeliYuk sampai waktu yang tidak ditentukan apabila terdapat permasalahan dan klaim dari pihak Pembeli terkait proses pengiriman dan kualitas Barang. Pembayaran baru akan dilanjut kirimkan kepada Penjual apabila permasalahn tersebut telah selesai dan/atau Barang telah diterima oleh Pembeli.</li>
	        	<li>Penjual memahami dan menyetujui bahwa Pajak Penghasilan Penjual akan dilaporkan dan diurus sendiri oleh masing-masing Penjual sesuai dengan ketentuan pajak yang berlaku di peraturan perundang-undangan di Indonesia.</li>
	        	<li>JualBeliYuk berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Penjual dan Pembeli, dengan melihat bukti-bukti yang ada. Keputusan JualBeliYuk adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Penjual dan Pembeli untuk mematuhinya.</li>
	        </ol>
	        
	        <h3>E.	HARGA</h3>
	        <ol>
	        	<li>Harga Barang yang terdapat dalam situs JualBeliYuk adalah harga yang ditetapkan oleh Penjual. Penjual dilarang memanipulasi harga barang dengan cara apapun.</li>
	        	<li>Pembeli memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak terbaharuinya halaman situs JualBeliYuk dikarenakan browser/ISP yang dipakai Pembeli adalah tanggung jawab Pembeli.</li>
	        	<li>Penjual memahami dan menyetujui bahwa kesalahan ketik yang menyebabkan keterangan harga atau informasi lain menjadi tidak benar/sesuai adalah tanggung jawab Penjual. Perlu diingat dalam hal ini, apabila terjadi kesalahan pengetikan keterangan harga Barang yang tidak disengaja, Penjual berhak menolak pesanan Barang yang dilakukan oleh pembeli.</li>
	        	<li>Pengguna memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat ketidaksepahaman antara Penjual dan Pembeli tentang harga bukanlah merupakan tanggung jawab JualBeliYuk.</li>
	        	<li>Situs JualBeliYuk untuk saat ini hanya melayani transaksi jual beli Barang dalam mata uang Rupiah.</li>
	        </ol>
	        
	        <h3>F.	TARIF PENGIRIMAN</h3>
	        <p>Setelah pembeli melakukan “PESAN BARANG” maka pembeli akan menerima notifikasi email jumlah tarif pengiriman untuk barang tersebut. Setelah itu baru pembeli melakukan pembayaran atau konfirmasi pembayaran yang sudah di jumlahkan dengan tarif pengiriman.</p>
	        
	        <h3>G.	KONTEN</h3>
	        <ol>
	        	<li>Dalam menggunakan layanan Situs JualBeliYuk, Pengguna dilarang untuk menggunakan konten yang mengandung unsur SARA, diskriminasi, merendahkan/menyudutkan orang lain, bahasa vulgar/seksual, dan bersifat ancaman.</li>
	        	<li>Pengguna dilarang mempergunakan foto/gambar Barang yang memiliki watermark yang menandakan hak kepemilikan orang lain.</li>
	        	<li>Penguna dengan ini memahami dan menyetujui bahwa penyalahgunaan foto/gambar yang di unggah oleh Pengguna adalah tanggung jawab Pengguna secara pribadi.</li>
	        	<li>Penjual tidak diperkenankan untuk mempergunakan foto/gambar Barang atau logo toko sebagai media untuk beriklan atau melakukan promosi ke situs-situs lain diluar Situs JualBeliYuk, atau memberikan data kontak pribadi untuk melakukan transaksi secara langsung kepada pembeli / calon pembeli.</li>
	        	<li>Ketika Pengguna menggungah ke Situs JualBeliYuk dengan konten atau posting konten, Pengguna memberikan JualBeliYuk hak non-eksklusif, di seluruh dunia, secara terus-menerus, tidak dapat dibatalkan, bebas royalti, disublisensikan ( melalui beberapa tingkatan ) hak untuk melaksanakan setiap dan semua hak cipta, publisitas , merek dagang , hak basis data dan hak kekayaan intelektual yang Pengguna miliki dalam konten, di media manapun yang dikenal sekarang atau di masa depan. Selanjutnya , untuk sepenuhnya diizinkan oleh hukum yang berlaku , Anda mengesampingkan hak moral dan berjanji untuk tidak menuntut hak-hak tersebut terhadap JualBeliYuk.</li>
	        	<li>Pengguna menjamin bahwa tidak melanggar hak kekayaan intelektual dalam mengunggah konten Pengguna kedalam situs JualBeliYuk. Setiap Pengguna dengan ini bertanggung jawab secara pribadi atas pelanggaran hak kekayaan intelektual dalam mengunggah konten di Situs JualBeliYuk.</li>
	        	<li>Meskipun kami mencoba untuk menawarkan informasi yang dapat diandalkan, kami tidak bisa menjanjikan bahwa katalog akan selalu akurat dan up-to -date, dan Pengguna setuju bahwa Pengguna tidak akan meminta JualBeliYuk bertanggung jawab atas ketimpangan dalam katalog. Katalog mungkin termasuk hak cipta, merek dagang atau hak milik lainnya.</li>
	        </ol>
	        
	        <h3>H.	JENIS BARANG</h3>
	        <ul>
	        	<p>Berikut ini adalah daftar jenis Barang yang dilarang untuk diperdagangkan di Situs JualBeliYuk:</p>
	        	<li>Segala jenis kosmetik, obat-obatan, makanan, dan minuman yang membahayakan kesehatan dan keselamatan penggunanya, serta tanpa izin edar dari Badan Pengawas Obat dan Makanan (BPOM).</li>
	        	<li>Media berbentuk CD/DVD/VCD, atau media rekam lain yang bertentangan dengan undang-undang hak cipta. Termasuk di dalamnya adalah yang memuat film, musik, permainan, atau perangkat lunak bajakan.</li>
	        	<li>Barang dewasa penunjang kegiatan seksual, termasuk di dalamnya obat-obatan penunjang, alat penunjang seks, pornografi, serta jenis Barang lainnya dalam kategori Barang dewasa.</li>
	        	<li>Iklan.</li>
	        	<li>Segala bentuk tulisan yang dapat berpengaruh negatif terhadap pemakaian situs ini.</li>
	        	<li>Senjata api, senjata tajam, dan segala macam senjata.</li>
	        	<li>Dokumen pemerintahan dan perjalanan.</li>
	        	<li>Seragam pemerintahan.</li>
	        	<li>Bagian/Organ manusia.</li>
	        	<li>Mailing list dan informasi pribadi.</li>
	        	<li>Barang-Barang yang melecehkan pihak/ras tertentu atau dapat merendahkan martabat orang lain.</li>
	        	<li>Pestisida.</li>
	        	<li>Atribut kepolisian.</li>
	        	<li>Barang hasil tindak pencurian.</li>
	        	<li>Pembuka kunci dan segala aksesori penunjang tindakan perampokan/pencurian.</li>
	        	<li>Surat, Warkat Pos dan atau Kartu Pos.</li>
	        	<li>Barang yang dapat dan atau mudah meledak, menyala atau terbakar sendiri.</li>
	        	<li>Barang cetakan/rekaman yang isinya dapat mengganggu keamanan & ketertiban serta stabilitas nasional.</li>
	        	<li>Uang tunai.</li>
	        	<li>Perlengkapan dan peralatan judi.</li>
	        	<li>Jimat-jimat, benda-benda yang diklaim berkekuatan gaib dan memberi ilmu kesaktian.</li>
	        	<li>Barang dengan hak Distribusi Eksklusif yang hanya dapat diperdagangkan dengan sistem penjualan lansung oleh penjual resmi dan/atau Barang dengan sistem penjualan Multi Level Marketing.</li>
	        	<li>Segala jenis Barang lain yang bertentangan dengan peraturan pengiriman Barang Indonesia.</li>
	        	<li>Barang-Barang lain yang melanggar ketentuan hukum yang berlaku di Indonesia.</li>
	        </ul>
	        
	        <h3>I. IKLAN</h3>
	        <ol>
	        	<li>Pengguna memahami dan menyetujui bahwa setiap iklan yang Pengguna unggah di situs JualBeliYuk adalah tanggung jawab Pengguna secara pribadi dan melepaskan JualBeliYuk dari segala permasalahan yang mungkin timbul daripadanya.</li>
	        	<li>JualBeliYuk tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk melakukan tindakan penghapusan atas setiap konten iklan yang melanggar Syarat & ketentuan Tokopedia dan/atau hukum yang berlaku.</li>
	        	<li>Pengguna dengan ini memahami dan menyetujui bahwa setiap kerugian yang timbul dari tindakan penghapusan iklan yang dilakukan oleh JualBeliYuk terhadap iklan yang dididuga melanggar bukanlah tanggung jawab JualBeliYuk.</li>
	        </ol>
	        

	        <h3>J. PENGIRIMAN BARANG</h3>
	        <ol>
	        	<li>Setiap ketentuan berkenaan dengan proses pengiriman Barang / Barang adalah wewenang sepenuhnya penyedia jasa layanan pengiriman Barang.</li>
	        	<li>Pengguna memahami dan menyetujui bahwa setiap permasalahan yang terjadi pada saat proses pengiriman Barang oleh penyedia jasa layanan pengiriman Barang adalah merupakan tanggung jawab penyedia jasa layanan pengiriman.</li>
	        </ol>
	        

	        <h3>K. PENARIKAN DANA</h3>
	        <ol>
	        	<li>Penarikan dana akan diproses dalam 2x24 jam hari kerja.</li>
	        	<li>Untuk penarikan dana dengan tujuan nomor rekening di luar bank BCA, apabila ada biaya tambahan yang dibebankan akan menjadi tanggungan dari Pengguna.</li>
	        </ol>
	        

	        <h3>L. PEMBAHARUAN</h3>
	        <p>Syarat & ketentuan mungkin di ubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. JualBeliYuk menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat & ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan JualBeliYuk, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & ketentuan.</p>
	    </div>
	</div>
</div>