<div id="content">
    <div class="container">
        <div class="contact">
            <div class="head-title">
                <h3 class="section-title">contact us</h3>
            </div>
            <form id="contact" method="POST">
                <div class="row-fluid">
                    <label class="control-label">Alamat Email</label>
                    <div class="controls">
                        <input autofocus="autofocus" type="text" id="email" name="email" class="input-text">
                    </div>
                </div>
                <div class="row-fluid">
                    <label class="control-label">Nama</label>
                    <div class="controls">
                        <input autofocus="autofocus" type="text" id="name" name="name" class="input-text">
                    </div>
                </div>
                <div class="row-fluid">
                    <textarea name="message" id="" cols="30" rows="10"></textarea>
                </div>

                <div class="row-fluid">
                    <p class="button">
                        <input type="submit" value="Submit" class="submit yellow button" style="border: none;">
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>