<?php
	//Main view to render header and footer as well as rendering spesific page that's contained in $content
    $error_message = $this->session->flashdata('error_message');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Jualbeliyuk</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php assets_url(); ?>style.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
    <script src="<?php assets_url(); ?>js/modernizr.js"></script>
</head>
<body data-base_url="<?php echo base_url(); ?>">
    <header id="header">
        <div class="top-header">
            <div class="container">
                <div class="text-info">Website ini sedang dalam pengembangan, untuk kritik & saran bisa menghubungi kami di Contact Us</div>
                <div class="socmed">
                    <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="<?php echo base_url().'dashboard/' ?>" class="user"><i class="fa fa-user"></i></a>
                </div><!-- .socmed -->
            </div>
        </div>
        <div class="container">
            <h1 id="site-logo">
                <a href="<?php echo base_url(); ?>">
                    <img src="<?php assets_url(); ?>img/oggend.png" alt="">
                </a>
            </h1><!-- #header-logo -->
            <div class="header-search">
                <div class="wrapper">
                    <form id="search" action="<?php echo base_url().'search'; ?>" method="POST">
                        <input type="search" id="searchInput" name="search" placeholder="Cari barang yang kamu mau...">
                        <select id="kategori" name="category_search">
                            <option value="hide">pilih kategori</option>
                            <?php foreach($categories as $category): ?>
                            <option value="<?php echo $category->category_id; ?>"><?php echo $category->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select id="lokasi" name="location_search">
                            <option value="hide" selected>pilih lokasi</option>
                            <?php foreach($locations as $location): ?>
                            <option value="<?php echo $location->location_id; ?>"><?php echo $location->location_name; ?></option>
                            <?php endforeach; ?>
                        </select> 
                        <input type="submit" value="cari" class="submit button yellow" style="padding: 12px 40px">
                    </form>
                </div>

            </div>
        </div>
    </header>
        <div class="alert">
            <div class="notify <?php echo (!empty($error_message)) ? 'active' : ''; ?>">
                <span id="notifyType" class="<?php echo (!empty($error_message)) ? 'failure' : '';?>"><?php echo (!empty($error_message)) ? $error_message : '';?></span>
            </div>
        </div>
    <?php $this->view($content); ?>
    <footer id="footer">
        <div class="container">
            <ul>
                <li><a href="<?php echo base_url()."misc/apa_jby"; ?>">Apa itu JBY</a></li>
                <li><a href="<?php echo base_url()."misc/terms"; ?>">Term & Condition</a></li>
                <!-- <li><a href="#">Mengapa JBY</a></li>
                <li><a href="#">Sitemap</a></li> -->
                <li><a href="<?php echo base_url()."misc/how"; ?>">How to</a></li>
                <li><a href="<?php echo base_url()."misc/contact"; ?>">contact us</a></li>
            </ul>
        </div>
    </footer>

    <script src="<?php assets_url(); ?>js/jquery.js"></script>
    <script src="<?php assets_url(); ?>js/plugins.js"></script>
    <script src="<?php assets_url(); ?>js/methods.js"></script>

</body>
</html>