<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?php assets_url(); ?>admin/images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?php assets_url(); ?>admin/images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			
				<!--  start message-yellow -->
				<div id="message-yellow" style="display: none;">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="yellow-left">You have a new message. <a href="">Go to Inbox.</a></td>
					<td class="yellow-right"><a class="close-yellow"><img src="<?php assets_url(); ?>admin/images/table/icon_close_yellow.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
				<!--  end message-yellow -->
				
				<!--  start message-red -->
				<div id="message-red" style="display: none;">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="red-left">Error. <a href="">Please try again.</a></td>
					<td class="red-right"><a class="close-red"><img src="<?php assets_url(); ?>admin/images/table/icon_close_red.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
				<!--  end message-red -->
				
				<!--  start message-blue -->
				<div id="message-blue" style="display: none;">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left">Welcome back. <a href="">View my account.</a> </td>
					<td class="blue-right"><a class="close-blue"><img src="<?php assets_url(); ?>admin/images/table/icon_close_blue.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
				<!--  end message-blue -->
			
				<!--  start message-green -->
				<div id="message-green" style="display: none;">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left"></td>
					<td class="green-right"><a class="close-green"><img src="<?php assets_url(); ?>admin/images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
				<!--  end message-green -->
		
		 
				<!--  start product-table ..................................................................................... -->
				<form id="mainform" action="">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Pembeli</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Alamat pembeli</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Alamat penjual</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Bukti pembayaran</a></th>
					<th class="table-header-repeat line-left"><a href="">Detail Produk</a></th>
					<th class="table-header-repeat line-left"><a href="">Tanggal transaksi</a></th>
					<th class="table-header-repeat line-left"><a href="">Status</a></th>
					<th class="table-header-repeat line-left"><a href="">Action</a></th>
				</tr>
				<?php if($transactions): foreach($transactions as $transaction): ?>
				<tr>
					<td><a href="<?php echo base_url().'user/'.$transaction->buyer_detail->username; ?>"><?php echo $transaction->buyer_detail->fullname; ?></a></td>
					<td><?php echo $transaction->buyer_detail->address; ?></td>
					<td><?php echo $transaction->seller_detail->address; ?></td>
					<td><a target="_blank" href="<?php echo base_url().'images/confirmation/payment/'.$transaction->payment_image; ?>">Foto</a></td>
					<td><a href="<?php echo base_url().'ads/detail/'.$transaction->item_detail->permalink; ?>">Detail</a></td>
					<td><?php echo $transaction->item_detail->booked_time; ?></td>
					<td class="status_<?php echo $transaction->item_id ?>"><?php echo ($transaction->payment_approval == 0) ? 'Belum disetujui' : 'Telah disetujui' ; ?></td>
					<td><a href="#" class="payment_action payment_<?php echo $transaction->item_id ?>" booking_token="<?php echo $transaction->booking_token ?>" item_id="<?php echo $transaction->item_id ?>" ><?php echo ($transaction->payment_approval == 0) ? 'Setujui' : 'Batalkan' ; ?></a></td>
					<!-- <td class="options-width">
					<a href="" title="Edit" class="icon-1 info-tooltip"></a>
					<a href="" title="Edit" class="icon-2 info-tooltip"></a>
					<a href="" title="Edit" class="icon-3 info-tooltip"></a>
					<a href="" title="Edit" class="icon-4 info-tooltip"></a>
					<a href="" title="Edit" class="icon-5 info-tooltip"></a>
					</td> -->
				</tr>
				<?php endforeach; endif;?>
				</table>
				<!--  end product-table................................... --> 
				</form>
			</div>
			<!--  end content-table  -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
				<tbody><tr>
					<td>
						<?php back_pagination($pagination) ?>
					</td>
					</tr>
				</tbody>
			</table>
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->