<div class="col-8 pull-right">
    <div class="edit-profile">
        <div class="head-title">
            <h3 class="section-title">Konfirmasi pembayaran</h3>
        </div>
        <div class="row">
            <form method="POST" class="edit-profile-form" enctype="multipart/form-data">
                <div class="row-fluid">
                    <label class="control-label">Bukti transfer</label>
                    <div class="controls">
                        <input type="file" name="trans_pic" />
                    </div>
                </div>
                <div class="row-fluid">
                    <label class="control-label">Token konfirmasi</label>
                    <div class="controls">
                        <input autofocus="autofocus" type="text" id="token" name="token" class="input-text">
                    </div>
                </div>
                <input type="submit" class="yellow button" value="Konfirmasi">
            </form>
        </div>
    </div>
</div>
