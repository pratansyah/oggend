<div class="col-8 pull-right">
    <div class="edit-profile">
        <div class="head-title">
            <h3 class="section-title">Konfirmasi barang sampai</h3>
        </div>
        <div class="row">
            <form method="POST" class="edit-profile-form" enctype="multipart/form-data">
                <div class="row-fluid">
                    <label class="control-label">Rating</label>
                    <div class="controls">
                        <select name="rating">
                            <option value='0'>Beri rating</option>
                            <option value='1'>1</option>
                            <option value='2'>2</option>
                            <option value='3'>3</option>
                            <option value='4'>4</option>
                            <option value='5'>5</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <label class="control-label">Token konfirmasi</label>
                    <div class="controls">
                        <input autofocus="autofocus" type="text" id="token" name="token" class="input-text">
                    </div>
                </div>
                <input type="submit" class="yellow button" value="Konfirmasi">
            </form>
        </div>
    </div>
</div>
