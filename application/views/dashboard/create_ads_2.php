<div id="content">
    <div class="container">
        <div class="kategori-iklan closed" style="display: none;">
            <a href="#" class="close"></a>
            <div class="col-4">
                <ul class="category-level1">
                    <?php foreach($categories as $category): ?>
                    <li><a href="#" data-key="<?php echo $category->category_id; ?>"><font><?php echo $category->name; ?></font></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-4">
                <ul class="category-level2">

                </ul>
            </div>
            <div class="col-4">
                <ul class="category-level3">

                </ul>
            </div>
        </div>
        <div class="pasang-iklan">
            <p class="copy-iklan">Pasang Iklan di JBY sepenuhnya GRATIS, kami tetap tidak memungut bayaran walaupun produk Anda laku.</p>
            <div class="col-2 pull-left detail-iklan">
                <form action="" class="iklan-form">
                    <div class="row-fluid">
                        <label class="control-label">Judul</label>
                        <div class="controls">
                            <input autofocus="autofocus" type="text" id="judul" name="judul" class="input-text" placeholder="judul iklan anda">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Harga</label>
                        <div class="controls">
                            <h3>Rp.</h3>
                            <input autofocus="autofocus" type="text" id="harga" name="harga" class="input-text harga" placeholder="masukan harga terbaikmu">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Provinsi</label>
                        <div class="controls">
                            <select id="provinsi" name="provinsi" data-function="load_kabupaten">
                                <option value="hide">pilih provinsi</option>
                                <?php foreach($locations as $location): ?>
                                <option data-province_code="<?php echo $location->province_code; ?>" value="<?php echo $location->location_id; ?>"><?php echo $location->location_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid kabupaten_row" style="display:none;">
                        <label class="control-label">Kabupaten/Kota</label>
                        <div class="controls">

                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">kategori</label>
                        <div class="controls">
                            <a href="#" class="pilih-kategori">pilih kategori</a>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">Bisa dikirim</label>
                        <div class="controls">
                            <select id="tipe" name="shipped" data-function="assign_value" data-id="shipped">
                                <option value="hide">pilih tipe</option>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label">kondisi</label>
                        <div class="controls">
                            <select id="kondisi" name="kondisi"  data-function="assign_value" data-id="kondisi">
                                <option value="hide">pilih kondisi</option>
                                <option value="baru" rel="icon-temperature">baru</option>
                                <option value="bekas">bekas</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <label class="control-label  deskripsi-iklan">Deskripsi Iklan</label>
                        <div class="controls text-editor">
                            <textarea name="deskripsi" id="" cols="30" rows="10" class="input-text textarea" placeholder="jelaskan detail produk Anda"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-2 pull-left iklan-images">
                <h2>unggah foto</h2>
                <p>Masukkan foto/gambar dari produk Anda, untuk respon yang jauh lebih baik dari calon pembeli</p>
                <p>Ukuran gambar maksimal 100kb dengan dimensi maksimal 1000x1000</p>
                <form action="" class="uploader" enctype="multipart/form-data" method="POST">
                    <div class="row-fluid">
                        <input type="file" name="pic[]" accept="image/*"/>
                        <input type="file" name="pic[]" accept="image/*"/>
                        <input type="file" name="pic[]" accept="image/*"/>
                        <input type="file" name="pic[]" accept="image/*"/>
                        <input type="file" name="pic[]" accept="image/*"/>
                        <input type="file" name="pic[]" accept="image/*"/>
                        <input type="hidden" name="title" id="hidden_judul"/>
                        <input type="hidden" name="harga" id="hidden_harga"/>
                        <input type="hidden" name="deskripsi" id="hidden_deskripsi"/>
                        <input type="hidden" name="kategori" id="hidden_kategori"/>
                        <input type="hidden" name="lokasi" id="hidden_lokasi"/>
                        <input type="hidden" name="shipped" id="hidden_shipped"/>
                        <input type="hidden" name="kondisi" id="hidden_kondisi"/>
                    </div>
                    <div class="iklan-action" style="margin-left: -25%; width: 40%; margin-top: 85%">
                        <input type="submit" value="pasang iklan" class="submit button yellow">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>