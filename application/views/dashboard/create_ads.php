
        <div class="col-8 pull-right">
            <div class="edit-profile">
                <div class="head-title">
                    <h3 class="section-title">Pasang iklan</h3>
                </div>
                <div class="row">

                        <form method="POST" class="edit-profile-form" enctype="multipart/form-data">
                            <div class="row-fluid">
                                <label class="control-label">Judul</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="title" name="title" class="input-text" >
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Kategori</label>
                                <div class="controls">
                                    <select id="category" name="category">
                                        <option value="hide">pilih kategori</option>
                                        <?php foreach($categories as $category): ?>
                                        <option value="<?php echo $category->category_id; ?>"><?php echo $category->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid radio">
                                <label class="control-label">Tipe</label>
                                <input type="radio" id="r1" name="type" value="dijual">
                                <label for="r1"><span></span>Dijual</label>
                                
                                <input type="radio" id="r2" name="type" value="dicari" >
                                <label for="r2"><span></span>Dibeli</label>

                                <input type="radio" id="r3" name="type" value="disewakan" checked>
                                <label for="r3"><span></span>Disewakan</label>

                                <input type="radio" id="r4" name="type" value="jasa" checked>
                                <label for="r4"><span></span>Jasa</label>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Harga</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="price" name="price" class="input-text" >
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Deskripsi</label>
                                <div class="controls">
                                    <textarea name="description"></textarea>
                                </div>
                            </div>
                            <div class="row-fluid radio">
                                <label class="control-label">Kondisi barang</label>
                                <input type="radio" id="r1" name="condition" value="Baru">
                                <label for="r1"><span></span>Baru</label>
                                
                                <input type="radio" id="r2" name="condition" value="Bekas" checked>
                                <label for="r2"><span></span>Bekas</label>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Provinsi</label>
                                <div class="controls">
                                    <select id="lokasi" class="province_profile" name="province_location">
                                        <option value="hide">pilih lokasi</option>
                                        <?php foreach($locations as $location): ?>
                                        <option data-province_code="<?php echo $location->province_code; ?>" value="<?php echo $location->location_id; ?>"><?php echo $location->location_name; ?></option>
                                        <?php endforeach; ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="row-fluid kabupaten_row">
                                <label class="control-label">Kota/Kabupaten</label>
                                <div class="controls">
                                    <select id="lokasi" class="city_profile">
                                        <option value="hide">pilih lokasi</option>

                                    </select> 
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Gambar produk 1</label>
                                <div class="controls">
                                    <input type="file" name="product_picture" />
                                </div>
                            </div>
                            <input type="submit" class="yellow button" value="simpan">
                        </form>
                </div>
            </div>
        </div>
