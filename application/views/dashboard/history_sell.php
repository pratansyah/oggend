<div class="col-8 pull-right">
    <div class="iklan-saya">
        <ul class="view-list">
            <?php foreach($items as $item): ?>
            <li>
                <figure>
                   <img src="<?php echo items_url().'thumb/'; echo (isset($item->image[0]->image)) ? $item->image[0]->image : 'default.png'; ?>" alt="">
                </figure>
                <div class="summary">
                    <span class="product-title"><?php echo $item->title; ?></span>
                    <div class="detail-summary">
                        <span class="price">Rp. <?php echo number_format($item->price, 0, ',', '.'); ?></span>
                        <span class="price">Token: <?php echo $item->sent_token; ?></span>
                        <span class="status"><?php echo ($item->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                    </div>
                </div>
                <div class="status-iklan">
                    <?php echo ($item->payment_approval == 0) ? 'Telah dipesan' : 'Telah dibayar'; ?>
                </div>
                <a href="<?php echo base_url().'dashboard/confirmation/send/'.$item->permalink; ?>" class="edit-iklan">Konfirmasi pengiriman</a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div><!-- End .catalog-products -->
</div>