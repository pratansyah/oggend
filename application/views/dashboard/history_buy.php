<div class="col-8 pull-right">
    <div class="iklan-saya">
        <ul class="view-list">
            <?php foreach($items as $item): ?>
            <?php
                $status = 'Belum dibayar';
                $link = 'Konfirmasi pembayaran';
                $url = base_url().'dashboard/confirmation/payment/'.$item->permalink;
                if($item->arrival_status == 1) {
                    $status = 'Telah sampai';
                    $link = '';
                    $url = '';
                }
                elseif($item->sent_image != '') {
                    $status = 'Sudah dikirim';
                    $link = 'Konfirmasi kedatangan';
                    $url = base_url().'dashboard/confirmation/arrival/'.$item->permalink;
                }
                elseif($item->payment_image != '' && $item->sent_token != '') {
                    $status = 'Sudah dibayar';
                    $link = '';
                    $url = '';
                }
            ?>
            <li>
                <figure>
                   <img src="<?php echo items_url().'thumb/'; echo (isset($item->image[0]->image)) ? $item->image[0]->image : 'default.png'; ?>" alt="">
                </figure>
                <div class="summary">
                    <span class="product-title"><?php echo $item->title; ?></span>
                    <div class="detail-summary">
                        <span class="price">Rp. <?php echo number_format($item->price, 0, ',', '.'); ?></span>
                        <span class="price">Token: <?php echo $item->payment_token; ?></span>
                        <span class="status"><?php echo ($item->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                    </div>
                </div>
                <div class="status-iklan">
                    <?php echo $status; ?>
                </div>
                <a href="<?php echo $url; ?>" class="edit-iklan"><?php echo $link; ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div><!-- End .catalog-products -->
</div>