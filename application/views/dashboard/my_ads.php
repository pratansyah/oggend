<div class="col-8 pull-right">
    <div class="iklan-saya">
        <ul class="view-list">
            <?php foreach($items as $item): ?>
            <li>
                <a href="<?php echo base_url().'ads/detail/'.$item->permalink; ?>">
                    <figure>
                       <img src="<?php echo items_url().'thumb/'; echo (isset($item->images[0]->image)) ? $item->images[0]->image : 'default.png'; ?>" alt="">
                    </figure>
                    <div class="summary">
                        <span class="product-title"><?php echo $item->title; ?></span>
                        <div class="detail-summary">
                            <span class="price">Rp. <?php echo number_format($item->price, 0, ',', '.'); ?></span>
                            <span class="status"><?php echo ($item->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                        </div>
                    </div>
                    <div class="status-iklan">
                        <?php echo ($item->active === 'f') ? 'Terjual' : 'Aktif'; ?>
                    </div>     
                    <a href="#" class="edit-iklan" data-permalink="<?php echo $item->permalink; ?>" data-item_id="<?php echo $item->item_id; ?>">
                        <div class="edit">edit-iklan</div>
                        <div class="delete">Hapus-iklan</div>
                    </a>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div><!-- End .catalog-products -->
    <a href="<?php echo base_url().'dashboard/create_ads'; ?>" class="button orange">Pasang Iklan</a>

</div>