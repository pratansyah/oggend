
        <div class="col-8 pull-right">
            <div class="edit-profile">
                <div class="head-title">
                    <h3 class="section-title">edit profile</h3>
                </div>
                <div class="row">
                    <div class="col-left">
                        <div class="avatar">
                            <img src="<?php echo avatar_url().$user->avatar; ?>" />
                        </div>
                    </div>
                    <div class="col-right">
                        <form method="POST" class="edit-profile-form" enctype="multipart/form-data">
                            <div class="row-fluid">
                                <label class="control-label">Nama Lengkap</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="full-name" name="full_name" class="input-text" value="<?php echo $user->fullname; ?>">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Avatar</label>
                                <div class="controls">
                                    <input type="file" name="ava_pic" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">No Hp</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="no_hp" name="no_hp" class="input-text" value="<?php echo $user->contact; ?>">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Bank</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="bank" name="bank" class="input-text" value="<?php echo (isset($user->bank_name)) ? $user->bank_name : ''; ?>">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">No Rekening</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="no_rek" name="no_rek" class="input-text" value="<?php echo isset($user->account_number) ? $user->account_number: ''; ?>">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Provinsi</label>
                                <div class="controls">
                                    <select id="lokasi" class="province_profile" name="province_location" data-function="load_kabupaten">
                                        <option value="hide"><?php echo (isset($user->province) ? $user->province->location_name : "Pilih Provinsi")?></option>
                                        <?php foreach($locations as $location): ?>
                                        <option data-province_code="<?php echo $location->province_code; ?>" value="<?php echo $location->location_id; ?>"><?php echo $location->location_name; ?></option>
                                        <?php endforeach; ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="row-fluid kabupaten_row">
                                <label class="control-label">Kota/Kabupaten</label>
                                <div class="controls">
                                    <select id="lokasi" class="city_profile">
                                        <option value="hide"><?php echo (isset($user->location_name) && $user->location_name != 'nowhere'  ? $user->location_name : "Pilih Kota/Kabupaten")?></option>
                                        <?php if( isset($user->cities) ): foreach($user->cities as $location): ?>
                                        <option data-province_code="<?php echo $location->province_code; ?>" value="<?php echo $location->location_id; ?>"><?php echo $location->location_name; ?></option>
                                        <?php endforeach; endif;  ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Alamat</label>
                                <div class="controls address-wrap">
                                    <?php if($user->address == ''): ?>
                                        <div id="hidden-input" style="display: block;">
                                    <?php else: ?>
                                        <div id="hidden-address" style="display: block">
                                            <?php echo $user->address; ?>
                                            <a href="#" id="address-trigger">Edit</a>
                                        </div>
                                        <div id="hidden-input" style="display: none;">
                                    <?php endif;?>
                                            <input id="alamat" name="alamat" class="input-text"/><br/>
                                            RT <input id="rt" name="rt" class="input-text" style="width: 100px" />
                                            RW <input id="rw" name="rw" class="input-text" style="width: 100px" /><br/>
                                            Kelurahan <input id="kelurahan" name="kelurahan" class="input-text" style="width: 235px" /><br/>
                                            Kecamatan <input id="kecamatan" name="kecamatan" class="input-text" style="width: 225px" /><br/>
                                            Kode Pos <input id="kode_pos" name="kode_pos" class="input-text" style="width: 100px" /><br/>
                                        </div>
                                </div>
                                <input type="hidden" id="address" name="address" value="<?php echo $user->address; ?>"/>
                            </div>
                            <div class="row-fluid radio">
                                <label class="control-label">jenis-kelamin</label>
                                <input type="radio" id="r1" name="rgroup" <?php if($user->sex == 'M') echo 'checked'; ?> value="M">
                                <label for="r1"><span></span>laki-laki</label>
                                
                                <input type="radio" id="r2" name="rgroup" <?php if($user->sex == 'F') echo 'checked'; ?> value="F">
                                <label for="r2"><span></span>perempuan</label>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Alamat Email</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="text" id="email" name="email" class="input-text" value="<?php echo $user->email; ?>">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">Kata sandi</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="password" id="password" name="password" class="input-text">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="control-label">ulangi kata sandi</label>
                                <div class="controls">
                                    <input autofocus="autofocus" type="password" id="password_repeat" name="password_repeat" class="input-text">
                                </div>
                            </div>
                            <input type="submit" class="yellow button" value="simpan">
                        </form>
                    </div>
                </div>
            </div>
        </div>
