<div id="content">
    <div class="container">
        <div class="profile-seller">
            <div class="seller-profile">
                <figure class="avatar">
                    <img src="<?php echo avatar_url().$user->avatar; ?>" alt="">
                </figure>
                <div class="about-seller">
                    <span class="name"><?php echo $user->fullname; ?></span>
                    <span class="location"><?php echo ($user->location_name != 'nowhere') ? $user->location_name : ''; ?></span>
                    <div class="since"><span>member sejak:</span> <?php echo date('d M Y', strtotime($user->created)); ?></div>
                    <div class="star-rating" title="Rated 4.00 out of 5"><span style="width:<?php echo $rating; ?>%"><strong class="rating">4.00</strong> out of 5</span></div>
                </div>
            </div>
            <div class="iklan-seller tabs-wrap">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1">Iklan Aktif</label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2">Sold Item</label>
                
                <section id="content1" class="panel">
                    <div class="products">
                       
                        <div class="products-head">
                            <div class="view">
                                <span class="view-option thumbnail-view view-active"><i class="fa fa-th-large"></i></span>
                                <span class="view-option list-view"><i class="fa fa-th-list"></i></span>
                            </div>
                        </div><!-- End .products-head -->

                        <div class="catalog-products">
                            <ul class="view-thumbnail">
                                <?php foreach($user->items as $item): if($item->active == 't'): ?>
                                <li>
                                    <a href="<?php echo base_url().'ads/detail/'.$item->permalink; ?>">
                                        <figure>
                                            <img src="<?php echo items_url().'\\thumb\\'; echo (isset($item->images[0]->image)) ? $item->images[0]->image : 'default.png'; ?>" alt="">
                                        </figure>
                                        <div class="summary">
                                            <span class="product-title"><?php echo $item->title; ?></span>
                                            <div class="detail-summary">
                                                <span class="price">Rp. <?php echo number_format($item->price, 0, ',', '.'); ?></span>
                                                <span class="status"><?php echo ($item->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                                            </div>
                                        </div>
                                        <div class="footer-summary">
                                            <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($item->created), time()); ?></span>
                                            <span class="location"><i class="fa fa-map-marker"></i><?php echo $item->location_name; ?></span>
                                        </div>     
                                    </a>
                                </li>
                                <?php endif; endforeach; ?>
                                
                            </ul>
                        </div><!-- End .catalog-products -->
                    </div>
                </section>
                <section id="content2" class="panel">
                    <div class="products">
                       
                        <div class="products-head">
                            <div class="view">
                                <span class="view-option thumbnail-view view-active"><i class="fa fa-th-large"></i></span>
                                <span class="view-option list-view"><i class="fa fa-th-list"></i></span>
                            </div>
                        </div><!-- End .products-head -->

                        <div class="catalog-products">
                            <ul class="view-thumbnail">
                                <?php foreach($user->items as $item): if($item->active == 'f'): ?>
                                <li>
                                    <a href="<?php echo base_url().'ads/detail/'.$item->permalink; ?>">
                                        <figure>
                                            <img src="<?php echo items_url().'\\thumb\\'; echo (isset($item->images[0]->image)) ? $item->images[0]->image : 'default.png'; ?>" alt="">
                                        </figure>
                                        <div class="summary">
                                            <span class="product-title"><?php echo $item->title; ?></span>
                                            <div class="detail-summary">
                                                <span class="price">Rp. <?php echo number_format($item->price, 0, ',', '.'); ?></span>
                                                <span class="status"><?php echo ($item->condition === 1) ? 'Baru' : 'Bekas'; ?></span>
                                            </div>
                                        </div>
                                        <div class="footer-summary">
                                            <span class="update"><i class="fa fa-clock-o"></i> <?php echo time_span(strtotime($item->created), time()); ?></span>
                                            <span class="location"><i class="fa fa-map-marker"></i><?php echo $item->location_name; ?></span>
                                        </div>     
                                    </a>
                                </li>
                                <?php endif; endforeach; ?>
                            </ul>
                        </div><!-- End .catalog-products -->
                    </div>
                </section>
            </div>

        </div>
    </div>
</div>