<?php
	class Item_mdl extends CI_Model {
		private $_conn;
		private $_table = 'item';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->load->model('image_mdl');
			$this->load->model('user_mdl');
			$this->load->model('location_mdl');
			$this->load->model('category_mdl');
			$this->_conn = new Conn();
		}

		public function create_item($item) {
			return $this->_conn->save($item, $this->_table, TRUE);
		}

		public function update_item($item, $item_id) {
			return $this->_conn->edit($item, $this->_table, array('item_id' => $item_id));
		}

		public function get_items($user_id) {
			$result = $this->_conn->retrieve($this->_table, array('user_id' => $user_id), '', TRUE, 'created', 'desc', 12);
			foreach ($result as $item) {
				$item->images = $this->image_mdl->get_images($item->item_id);
				$location = $this->location_mdl->get_location($item->location_id);
				$item->location_name = $location->location_name;
			}
			return $result;
		}

		public function get_item_by_permalink($permalink, $user_id=0) {
			return $this->_conn->retrieve($this->_table, array('user_id' => $user_id, 'permalink' => $permalink));
		}

		public function get_item_detail($permalink, $ads_detail=TRUE) {
			$detail['item'] = $this->_conn->retrieve($this->_table, array('permalink' => $permalink));
			$detail['item']->image = $this->image_mdl->get_images($detail['item']->item_id);
			$location = $this->location_mdl->get_location($detail['item']->location_id);
			$category = $this->category_mdl->get_category_by_category_id($detail['item']->category_id);
			$detail['item']->category_slug = $category->slug;
			$detail['item']->location_name = $location->location_name;

			if($ads_detail == TRUE) {
				$detail['user'] = $this->user_mdl->get_user_by_user_id($detail['item']->user_id);
				$location = $this->location_mdl->get_location($detail['user']->location_id);
				$detail['user']->location_name = $location->location_name;
			}
			return $detail;
		}

		public function get_item_detail_by_id($item_id) {
			$detail= $this->_conn->retrieve($this->_table, array('item_id' => $item_id));
			$detail->image = $this->image_mdl->get_images($detail->item_id);
			$location = $this->location_mdl->get_location($detail->location_id);
			$category = $this->category_mdl->get_category_by_category_id($detail->category_id);
			$detail->category_slug = $category->slug;
			$detail->location_name = $location->location_name;

			return $detail;
		}

		public function get_items_by_category_id($category_id, $order="", $sort="", $limit=0, $offset=0, &$total_rows, $param="") {
			$category = array();
			if($category_id != 0) {
				$category = array('category_id' => $category_id);
			}
			$items = $this->_conn->retrieve($this->_table, array('active' => 't'), 'permalink', FALSE, $order, $sort, $limit, $offset, '', $param);
			$result = array();
			$counter = 0;
			if(count($items) > 1) {
				foreach ($items as $item) {
					$result[$counter] = $this->get_item_detail($item->permalink, FALSE);
					$counter++;
				}
			}
			elseif(count($items) == 1){
				$result[0] = $this->get_item_detail($items->permalink, FALSE);
			}
			else {
				$result = NULL;
			}
			$total_rows = $this->_conn->countRetrievedRows($this->_table, array('active' => 't'), "", "", "", "", "", $param);
			return $result;
		}

		public function get_newest_items() {
			$items = $this->_conn->retrieve($this->_table, array('active' => 't'), false, 'desc', 'created', 12);
			$result = array();
			if(count($items) > 1) {
				foreach ($items as $item) {
					$result[] = $this->get_item_detail($item->permalink, FALSE);
				}
			}
			return $result;
		}

		public function search($like, $where="", $order="", $sort="", $limit=10, $offset=0, &$total_rows) {
			$items = $this->_conn->search($this->_table, $like, $where, '', $order, $sort, $limit, $offset);
			$total_rows = count($this->_conn->search($this->_table, $like, $where));
			// var_dump($items);die();
			$result = FALSE;
			if($items) {
				foreach($items as $item) {
					$result[] = $this->get_item_detail($item->permalink, FALSE);
				}
			}
			return $result;
		}

		public function get_newest_items_by_category($category_id) {
			$items = $this->_conn->retrieve($this->_table, array('category_id' => $category_id), false, 'asc', 'created', 4);
			$result = array();
			if(count($items) > 1) {
				foreach ($items as $item) {
					$result[] = $this->get_item_detail($item->permalink, FALSE);
				}
			}
			return $result;
		}

		public function book_item($item_id, $token, $user_id) {
			$result = $this->_conn->edit(array('booked_by' => $user_id, 'booked_time' => date('Y-m-d H:i:s'), 'booked' => 't'), $this->_table, array('item_id' => $item_id, 'booking_token' => $token));
			return $result;
		}

		public function get_user_id($item_id) {
			$result = $this->_conn->retrieve($this->_table, array('item_id' => $item_id), 'user_id');
			if($result) {
				return $result->user_id;
			}
		}

		public function edit_item($item_obj, $param) {
			return $this->_conn->edit($item_obj, $this->_table, $param);
		}

		public function delete_item($param) {
			return $this->_conn->remove($this->_table, $param);
		}

		public function get_item_ids($user_id) {
			$item_obj = $this->_conn->retrieve($this->_table, array('user_id' => $user_id));
			$item_ids = FALSE;

			if($item_obj) {
				$item_ids = array();
				foreach($item_obj as $item) {
					$item_ids[] = $item->item_id;
				}
			}

			return $item_ids;
		}
	}
?>