<?php
	class Category_mdl extends CI_Model {
		private $_conn;
		private $_table = 'category';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function get_categories($parent = 0) {
			return $this->_conn->retrieve($this->_table, array('parent' => $parent));
		}

		public function get_all_category() {
			$root = $this->_conn->retrieve($this->_table, array('parent' => 0));
		}

		// public function convert_name_to_slug() {
		// 	$categories = $this->_conn->retrieve($this->_table);
		// 	foreach ($categories as $category) {
		// 		if($category->parent == 0) {
		// 			$this->_conn->edit(array('slug' => preg_replace('/[\-]+/', '-', preg_replace('/[^a-zA-Z0-9]/', '-', strtolower($category->name)))), $this->_table, array('category_id' => $category->category_id));
		// 		}
		// 		else {
		// 			$parent = $this->_conn->retrieve($this->_table, array('category_id' => $category->parent));
		// 			$parent_slug = preg_replace('/[\-]+/', '-', preg_replace('/[^a-zA-Z0-9]/', '-', strtolower($parent->name)));
		// 			$this->_conn->edit(array('slug' => $parent_slug.'-'.preg_replace('/[\-]+/', '-', preg_replace('/[^a-zA-Z0-9]/', '-', strtolower($category->name)))), $this->_table, array('category_id' => $category->category_id));
		// 		}
		// 	}
		// }

		public function get_category_id_by_slug($slug=false) {
			if(!$slug) {
				$category->name = "Kategori Utama";
				$category->category_id = 0;
				$category->parent = 0;
				$category->slug = '';
			}
			else{
				$category = $this->_conn->retrieve($this->_table, array('slug' => $slug));	
			}
			$child = $this->_conn->retrieve($this->_table, array('parent' => $category->category_id));
			if(!$child) {
				$child = $this->_conn->retrieve($this->_table, array('parent' => $category->parent));
			}
			$category->child = $child;
			$this->get_parent($category);
			return $category;
		}

		public function get_category_by_category_id($category_id) {
			return $this->_conn->retrieve($this->_table, array('category_id' => $category_id));
		}

		private function get_parent(&$category) {
			if($category->parent != 0){
				$category->parent_cat = $this->_conn->retrieve($this->_table, array('category_id' => $category->parent));
				$this->get_parent($category->parent_cat);
			}
			else {
				$category->parent_cat = NULL;
			}
		}

		public function get_all_childs($category_id) {
			$child = $this->_conn->retrieve($this->_table, array('parent' => $category_id));
			$result = array();
			if($child) {
				foreach ($child as $category) {
					$childs = $this->_conn->retrieve($this->_table, array('parent' => $category->category_id));
					if($childs) {
						foreach($childs as $childss) {
							$result[] = $childss->category_id;
						}
					}
					$result[] = $category->category_id;
				}
			}
			return $result;
		}

	}
?>