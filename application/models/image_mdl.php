<?php
	class Image_mdl extends CI_Model {
		private $_conn;
		private $_table = 'image';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add_image($item) {
			return $this->_conn->save($item, $this->_table, TRUE);
		}

		public function get_images($item_id) {
			return $this->_conn->retrieve($this->_table, array('item_id' => $item_id), '', TRUE);
		}

		public function delete_image($param) {
			return $this->_conn->remove($this->_table, $param);
		}

	}
?>