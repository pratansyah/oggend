<?php
class Location_mdl extends CI_Model{

	private $_conn;
	private $_table = 'location';

	function __construct(){
		parent::__construct();

		$this->load->library('conn');
		$this->_conn = new Conn();
	}
	

	function get_provinces() {
		return $this->_conn->retrieve($this->_table, array('city_code' => 0, 'province_code !=' => 0), array('location_id', 'location_code', 'location_name', 'province_code'));
	}

	function get_location($location_id) {
		return $this->_conn->retrieve($this->_table, array('location_id' => $location_id));
	}

	function get_cities($province_code) {
		return $this->_conn->retrieve($this->_table, array('location_code LIKE' => $province_code.'.%', 'location_code !=' => $province_code.'.00'));
	}

	function get_province_detail($province_code) {
		return $this->_conn->retrieve($this->_table, array('location_code' => $province_code.'00'));
	}
}
?>