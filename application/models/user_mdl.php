<?php
	class User_mdl extends CI_Model {
		private $_conn;
		private $_table = 'user';

		function __construct() {
			parent::__construct();

			$this->load->model('location_mdl');
			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		function create_user($user) {
			return $this->_conn->save($user, $this->_table, true);
		}

		function get_user_by_username($username) {
			$user = $this->_conn->retrieve($this->_table, array('username' => $username), array('email', 'sex', 'avatar', 'fullname', 'contact', 'location_id', 'user_id', 'created', 'bank_name', 'account_number', 'username', 'address'));
			$location = $this->location_mdl->get_location($user->location_id);
			$cities = $this->location_mdl->get_cities($location->province_code);
			$province = $this->location_mdl->get_province_detail(substr($location->location_code, 0, -2));
			$user->location_name = $location->location_name;
			$user->province = $province;
			$user->cities = $cities;
			return $user;
		}

		function get_user_by_user_id($user_id) {
			return $this->_conn->retrieve($this->_table, array('user_id' => $user_id), array('email', 'sex', 'avatar', 'fullname', 'contact', 'location_id', 'created', 'username', 'bank_name', 'account_number', 'address'));
		}

		function login($user) {
			return $this->_conn->retrieve($this->_table, $user, array('username', 'user_id'));
		}

		function update_user($user) {
			return $this->_conn->edit($user, $this->_table, array('username' => $this->session->userdata('username')));
		}

		function get_avatar_filename($username) {
			return $this->_conn->retrieve($this->_table, array('username' => $username), array('avatar'));
		}

		function check_username($username) {
			return $this->_conn->retrieve($this->_table, array('username' => $username));
		}

		function check_email($email) {
			return $this->_conn->retrieve($this->_table, array('email' => $email));
		}

		function get_users($param="", $order="created", $sort="DESC", $limit="", $offset="", &$total_rows) {
			$total_rows = $this->_conn->countRetrievedRows($this->_table, $param);
			return $this->_conn->retrieve($this->_table, $param, '', TRUE, $order, $sort, $limit, $offset);
		}

		function delete_user($param) {
			return $this->_conn->remove($this->_table, $param);
		}
	}
?>