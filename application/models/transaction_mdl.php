<?php
	class Transaction_mdl extends CI_Model {
		private $_conn;
		private $_table = 'transaction';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->load->model('item_mdl');
			$this->load->model('user_mdl');
			$this->_conn = new Conn();
		}

		public function add_transaction($item) {
			return $this->_conn->save($item, $this->_table, FALSE);
		}

		public function get_bought_items($user_id) {
			$items_id = $this->_conn->retrieve($this->_table, array('buyer_id' => $user_id), array('item_id', 'booking_token', 'payment_approval', 'arrival_status', 'payment_token', 'sent_image', 'payment_image', 'sent_token'), true);
			$result = array();
			foreach($items_id as $item_id) {
				$result[$item_id->item_id] = $this->item_mdl->get_item_detail_by_id($item_id->item_id);
				$result[$item_id->item_id]->payment_approval = $item_id->payment_approval;
				$result[$item_id->item_id]->arrival_status = $item_id->arrival_status;
				$result[$item_id->item_id]->payment_token = $item_id->payment_token;
				$result[$item_id->item_id]->sent_image = $item_id->sent_image;
				$result[$item_id->item_id]->payment_image = $item_id->payment_image;
				$result[$item_id->item_id]->sent_token = $item_id->sent_token;
			}

			return $result;
		}

		public function get_sold_items($user_id) {
			$items_id = $this->_conn->retrieve($this->_table, array('seller_id' => $user_id), array('item_id', 'booking_token', 'payment_approval', 'arrival_status', 'sent_token', 'sent_image'), true);
			$result = array();
			foreach($items_id as $item_id) {
				$result[$item_id->item_id] = $this->item_mdl->get_item_detail_by_id($item_id->item_id);
				$result[$item_id->item_id]->payment_approval = $item_id->payment_approval;
				$result[$item_id->item_id]->arrival_status = $item_id->arrival_status;
				$result[$item_id->item_id]->sent_token = $item_id->sent_token;
			}

			return $result;
		}

		public function confirm_token($param) {
			$result = $this->_conn->retrieve($this->_table, $param);
			if($result) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}

		public function update_trans($obj_update, $param) {
			return $this->_conn->edit($obj_update, $this->_table, $param);
		}

		public function get_transactions($param, $order="booked_time", $sort="DESC", $limit="", $offset="", &$total_rows) {
			$transactions = $this->_conn->retrieve($this->_table, $param, '', TRUE, $order, $sort, $limit, $offset);
			$result = array();
			if($transactions) {
				foreach ($transactions as $transaction) {
					$transaction->buyer_detail = $this->user_mdl->get_user_by_user_id($transaction->buyer_id);
					$transaction->seller_detail = $this->user_mdl->get_user_by_user_id($transaction->seller_id);
					$transaction->item_detail = $this->item_mdl->get_item_detail_by_id($transaction->item_id);
					$result[] = $transaction;
				}
			}
			else {
				return FALSE;
			}
			$total_rows = $this->_conn->countRetrievedRows($this->_table, $param);
			return $result;
		}

		public function get_transaction_detail($param) {
			return $this->_conn->retrieve($this->_table, $param);
		}

		public function get_user_rating($seller_id) {
			$this->db->where(array('seller_id' => $seller_id, 'review_value !=' => 0));
			$this->db->select_avg('review_value');
			$result = $this->db->get($this->_table)->row();
			return $result->review_value;
		}

		public function delete_transaction($param) {
			return $this->_conn->remove($this->_table, $param);
		}

		public function cron_terminate_booking() {
			// Initial query to take item that's booked for more than 3 hour
			$sql = "select item_id from item where booked_time < now() - interval 3 hour";
			$items = $this->_conn->nativeQuery($sql);

			$sql = "select item_id from transaction where item_id in(";
			foreach($items as $item) {
				$sql .= $item->item_id.',';
			}
			$sql = substr($sql, 0, -1);
			$sql .= ") and sent_token=''";
			// echo $sql;die();
			$item_transaction = $this->_conn->nativeQuery($sql);
			$sql = "delete from transaction where item_id in(";
			if($item_transaction) {
				foreach($item_transaction as $item) {
					$sql .= $item->item_id.',';
				}
				$sql = substr($sql, 0, -1);
				$sql .= ") and sent_token=''";
				$delete = $this->db->query($sql);
				if($delete) {
					$sql = "update item set booked = 'f' where item_id in(";
					foreach($item_transaction as $item) {
						$sql .= $item->item_id.',';
					}
					$sql = substr($sql, 0, -1);
					$sql .= ")";
					$update = $this->db->query($sql);
				}
				if($update && $delete) {
					echo "SUKSES";
				}
				else {
					echo "GAGAL MANING SON";
				}
			}

		}

		public function get_buyer_detail($user_id, $item_id) {
			$sql = "SELECT user.address, user.fullname, location.location_name, item.title, item.permalink
					FROM user, location, item
					WHERE user.user_id=$user_id
						AND item.item_id=$item_id
						AND location.location_id=user.location_id";
			return $this->_conn->nativeQuery($sql);
		}

	}
?>