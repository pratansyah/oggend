<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('assets_url()'))
{
	function assets_url()
	{
		echo base_url().'assets/';
	}
}
function avatar_url() {
	return base_url().'images/avatar/';
}

function items_url() {
	return base_url().'images/product/';
}

function time_span($past, $now) {
	$time = round(abs($now - $past)/60);
	if($time < 60 ) {
		return $time.' menit yang lalu';
	}
	else {
		$time = round($time/60);
		if($time < 24) {
			return $time.' jam yang lalu';
		}
		else {
			$time = round($time/24);
			if($time < 30) {
				return $time.' hari yang lalu';
			}
			else {
				$time = round($time/30);
				return $time.' bulan yang lalu';
			}
		}
	}
}

function front_pagination($config) {
	$url = (isset($_GET['sort'])) ? '?sort='.$_GET['sort'].'&page=' : '?page=';
	$total_page = (int)(ceil($config['total_rows']/$config['per_page']));
	$counter = ($config['current_page'] < 4) ? 1 : $config['current_page']-2;
	echo '<nav class="paging cl" role="navigation">';
	if($config['current_page'] > 1) {
		$prev = $config['current_page'] - 1;
		echo "<a href='$url.$prev' class='prev'>Prev</a>";
	}
	if($counter != 1) {
		echo "<span class='hellip'>&hellip;</span>";
	}
	for($counter = $counter;$counter < $config['current_page']; $counter++) {
		echo "<a href='$url$counter'>$counter</a>";
	}
	echo "<span class='current'>{$config['current_page']}</span>";
	$counter_stop = ($total_page > 3 && $config['current_page']+2 < $total_page) ? $config['current_page']+2 : $total_page;
	// echo $counter_stop;die();
	for($counter = $counter+1;$counter <= $counter_stop; $counter++) {
		echo "<a href='$url$counter'>$counter</a>";
	}
	if($counter_stop != $total_page) {
		echo "<span class='hellip'>&hellip;</span>";
	}
	if($config['current_page'] < $total_page){
		$next = $config['current_page'] + 1;
		echo "<a href='$url$next' class='next'>Next</a>";
	}
	echo '</nav>';
}

function back_pagination($config) {
	if($config['current_page']>1) {
		echo '<a href="?page='.($config['current_page']-1).'" class="page-left"></a>';
	}
	echo '<div id="page-info">Page <strong>'.$config['current_page'].'</strong> / '.$config['total_pages'].'</div>';
	if($config['current_page'] < $config['total_pages']) {
		echo '<a href="?page='.($config['current_page']+1).'" class="page-right"></a>';
	}
}